import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SimpleText extends StatelessWidget {
  String? text;
  double fontSize;
  FontWeight fontWeight;
  Color color;
  TextAlign? textAlign;
  bool nunito, playFairDisplay;
  double padding;
  VoidCallback? onTap;

  SimpleText(this.text,
      {Key? key,
      this.fontSize = 16,
      this.fontWeight = FontWeight.w500,
      this.color = blackColor,
      this.nunito = false,
      this.playFairDisplay = false,
      this.onTap,
      this.padding = 0,
      this.textAlign = TextAlign.start})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    init(context);
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.all(padding),
        child: Text(
          text!,
          overflow: TextOverflow.visible,
          textAlign: textAlign,
          style: nunito
              ? GoogleFonts.nunito(
                  textStyle: TextStyle(
                      color: color,
                      fontSize: width(fontSize),
                      fontWeight: fontWeight))
              : playFairDisplay
                  ? GoogleFonts.playfairDisplay(
                      textStyle: TextStyle(
                          color: color,
                          fontSize: width(fontSize),
                          fontWeight: fontWeight))
                  : GoogleFonts.poppins(
                      textStyle: TextStyle(
                          color: color,
                          fontSize: width(fontSize),
                          fontWeight: fontWeight)),
        ),
      ),
    );
  }
}
