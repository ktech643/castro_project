import 'package:bottom_picker/bottom_picker.dart';
import 'package:bottom_picker/resources/arrays.dart';
import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/simple_text.dart';
import 'package:flutter/material.dart';

class MyTimePicker extends StatefulWidget {
  const MyTimePicker({Key? key}) : super(key: key);

  @override
  _MyTimePickerState createState() => _MyTimePickerState();
}

class _MyTimePickerState extends State<MyTimePicker> {

  var finalTime = 'Select Time';
  bool isSubmit = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        // openTimePicker(context);
        BottomPicker.time(
          title: 'Set your next Booking time',
          titleStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: width(15),
            color: orangeColor,
          ),
          pickerTextStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: width(13),
            color: blackColor,
          ),
          onSubmit: (index) {
            print(index);
            List temp = index.toString().split(' ');
            String time = temp[1];
            List temp2 = time.toString().split(':');
            setState(() {
              isSubmit = true;
              finalTime = '${temp2[0]} : ${temp2[1]}';
              print('>>>>$finalTime');
            });
          },
          onClose: () {
          },
          bottomPickerTheme: BOTTOM_PICKER_THEME.orange,
          use24hFormat: true,
        ).show(context);
      },
      child: Container(
        height: height(54),
        width: double.infinity,
        decoration: BoxDecoration(
            color: greyLightColor3,
            borderRadius: BorderRadius.circular(width(10)),
            border: Border.all(color: borderColor)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SimpleText(
              finalTime,
              fontWeight: FontWeight.w500,
              fontSize: 14,
              color: blackColor,
            ),
            SizedBox(width: width(10)),
            isSubmit ? SizedBox() : Icon(Icons.arrow_drop_down)
          ],
        ),
      ),
    );
  }
}
