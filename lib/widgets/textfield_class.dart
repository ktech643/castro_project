import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class TextFieldClass extends StatefulWidget {
  FontWeight fontWeight;
  String? hintText, label;
  TextInputType? keyboardType;
  String? Function(String?)? validator;
  bool? obscureText;
  TextEditingController? textEditingController;
  double padding;
  Widget? suffix;
  VoidCallback? onTap;
  bool? enable;
  int? maxLength;
  bool dateField;
  Color? fillColor;
  bool isEmail, isFullname;
  void Function(String)? onFieldSubmitted;
  String? email, name;

  TextFieldClass(this.hintText,
      {Key? key,
      this.padding = 4,
      this.suffix,
      this.keyboardType,
      this.textEditingController,
      this.maxLength,
      this.validator,
      this.onFieldSubmitted,
      this.obscureText = false,
      this.onTap = null,
      this.isEmail = false,
      this.fillColor,
      this.isFullname = false,
      this.dateField = false,
      this.enable,
      this.label,
      this.fontWeight = FontWeight.normal})
      : super(key: key);

  @override
  State<TextFieldClass> createState() => _TextFieldClassState();
}

class _TextFieldClassState extends State<TextFieldClass> {
  @override
  Widget build(BuildContext context) {
    init(context);

    return TextFormField(
      textInputAction: TextInputAction.next,
      cursorColor: pinkColor,
      maxLength: widget.maxLength,
      enabled: widget.enable,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: widget.textEditingController,
      onFieldSubmitted: widget.onFieldSubmitted,
      keyboardType: widget.keyboardType,
      obscureText: widget.obscureText!,
      obscuringCharacter: '*',
      style: GoogleFonts.poppins(
          textStyle: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: width(16),
        color: blackColor,
      )),
      validator: widget.validator,
      decoration: InputDecoration(
        filled: true,
        suffixIcon: widget.suffix,
        fillColor: widget.fillColor ?? whiteColor,
        contentPadding: EdgeInsets.only(left: width(30)),
        labelText: widget.label,
        labelStyle: GoogleFonts.poppins(
            textStyle: TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 16,
          color: greyDarkColor,
        )),
        hintText: widget.hintText,
        hintStyle: GoogleFonts.poppins(
            textStyle: TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 16,
          color: greyDarkColor,
        )),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: const BorderSide(color: borderColor),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: const BorderSide(color: borderColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: const BorderSide(color: pinkColor),
        ),
      ),
    );
  }
}
