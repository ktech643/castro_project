import 'package:flutter/material.dart';

class SpecialistContainer extends StatelessWidget {
  final VoidCallback onTap;
  final String name;
  final String address;
  final String time;
  final Widget image;

  SpecialistContainer(
      {required this.onTap,
      required this.name,
      required this.address,
      required this.image,
      required this.time});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        children: [
          image,
          SizedBox(
            width: 15,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  address,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.grey),
                )
              ],
            ),
          ),
          Container(
            height: 25,
            width: 45,
            decoration: BoxDecoration(
                color: Colors.green.withOpacity(0.5),
                borderRadius: BorderRadius.all(Radius.circular(5))),
            child: Center(
                child: Text(
              time,
              style: TextStyle(color: Colors.white),
            )),
          )
        ],
      ),
    );
  }
}
