import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/simple_text.dart';
import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  String? text;
  VoidCallback? onTap;
  bool singleColor;
  bool buttonWithBorder;

  Button(this.text,
      {Key? key,
      this.onTap,
      this.singleColor = false,
      this.buttonWithBorder = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    init(context);
    return Card(
      color: Colors.transparent,
      elevation: 5,
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: height(49),
          width: double.infinity,
          decoration: singleColor
              ? BoxDecoration(
                  color: whiteColor,
                  borderRadius: BorderRadius.circular(width(10)),
                  border: Border.all(
                      color: buttonWithBorder ? pinkColor : Colors.transparent))
              : BoxDecoration(
                  gradient: const LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                      orangeColor2,
                      pinkColor,
                      orangeColor,
                    ],
                  ),
                  borderRadius: BorderRadius.circular(width(10)),
                ),
          child: Center(
              child: SimpleText(
            text,
            color: singleColor ? pinkColor : whiteColor,
          )),
        ),
      ),
    );
  }
}

class Button2 extends StatelessWidget {
  String? text, text2;
  VoidCallback? onTap;

  Button2(this.text, this.text2, {Key? key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    init(context);
    return Card(
      color: Colors.transparent,
      elevation: 5,
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: height(49),
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: const LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
                orangeColor2,
                pinkColor,
                orangeColor,
              ],
            ),
            // color: whiteColor,
            borderRadius: BorderRadius.circular(width(10)),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: width(20)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SimpleText(
                  'Subscribe',
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: whiteColor,
                ),
                SimpleText(
                  'Pay \$$text/ $text2',
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: whiteColor,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SmallButton extends StatelessWidget {
  String? text;
  VoidCallback? onTap;
  bool isAccept, isCompleted;

  SmallButton(this.text, {Key? key, this.onTap, this.isAccept = true, this.isCompleted = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    init(context);
    return Card(
      color: Colors.transparent,
      elevation: 5,
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: height(29.31),
          width: isCompleted ? width(120.51) : width(90.8),
          decoration: BoxDecoration(
            color: isAccept ? greenColor2 : redColor,
            borderRadius: BorderRadius.circular(width(5)),
          ),
          child: Center(
            child: SimpleText(
              text,
              fontSize: 10,
              color: whiteColor,
            ),
          ),
        ),
      ),
    );
  }
}

class CircularIndicatorBar extends StatelessWidget {
  CircularIndicatorBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    init(context);
    return Card(
      color: pinkColor,
      child: Container(
        height: height(51),
        width: double.infinity,
        child: Center(child: CircularProgressIndicator(color: whiteColor)),
      ),
    );
  }
}
