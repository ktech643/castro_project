import 'package:bottom_picker/bottom_picker.dart';
import 'package:bottom_picker/resources/arrays.dart';
import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/simple_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

class MyDatePicker extends StatefulWidget {
  bool isSmall;
  String text;

  MyDatePicker(
      {Key? key, this.isSmall = false, this.text = 'Select your date of birth'})
      : super(key: key);

  @override
  _MyDatePickerState createState() => _MyDatePickerState();
}

class _MyDatePickerState extends State<MyDatePicker> {
  var finalDate;
  bool isSubmit = false;

  @override
  Widget build(BuildContext context) {
    // print('isSubmit : $isSubmit');
    init(context);
    return InkWell(
      onTap: () {
        // openDatePicker(context);
        BottomPicker.date(
          title: 'Select Date',
          titleStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: width(15),
            color: orangeColor,
          ),
          dateOrder: DatePickerDateOrder.dmy,
          pickerTextStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: width(13),
            color: blackColor,
          ),
          onChange: (index) {
            print(index);
          },
          onSubmit: (index) {
            final dateFormat = DateFormat('dd MMM yyyy');
            // print('finalDate...${dateFormat.format(index)}');
            setState(() {
              isSubmit = true;
              finalDate = dateFormat.format(index);
              widget.text = finalDate;
            });
          },
          bottomPickerTheme: BOTTOM_PICKER_THEME.orange,
        ).show(context);
      },
      child: Container(
        height: widget.isSmall ? height(54) : height(49),
        width: double.infinity,
        decoration: BoxDecoration(
            color: widget.isSmall ? greyLightColor3 : whiteColor,
            borderRadius: BorderRadius.circular(width(10)),
            border: Border.all(color: borderColor)),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: width(18)),
          child: Row(
            mainAxisAlignment: widget.isSmall
                ? MainAxisAlignment.spaceEvenly
                : MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(left: width(10)),
                child: SimpleText(widget.text,
                    color: widget.isSmall ? blackColor : greyDarkColor,
                    fontSize: widget.isSmall ? 14 : 16),
              ),
              widget.isSmall
                  ? isSubmit
                      ? SizedBox(height: 5, width: 5)
                      : Icon(Icons.arrow_drop_down)
                  : SvgPicture.asset('assets/calendar2.svg'),
            ],
          ),
        ),
      ),
    );
  }
}
