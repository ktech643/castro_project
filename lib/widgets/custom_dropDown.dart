import 'package:castro_app/widgets/primaryColor.dart';
import 'package:flutter/material.dart';

class CustomDropdownWidget extends StatelessWidget {
  final String defaultValue;
  final String selectedValue;
  final Function(String?)? onValueChanged;
  final List<String> itemList;
  final Function validator;
  final FocusNode? focusNode;
  final GlobalKey? dropDownKey;
  final Color? selectedColor;

  CustomDropdownWidget({
    required this.defaultValue,
    required this.selectedValue,
    required this.onValueChanged,
    required this.itemList,
    required this.validator,
    this.selectedColor,
    this.focusNode,
    this.dropDownKey,
  });

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: DropdownButtonFormField(
        key: dropDownKey,
        focusNode: focusNode,
        isExpanded: true,
        value: selectedValue,
        decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: const BorderSide(color: borderColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: const BorderSide(color: pinkColor),
            ),
            isCollapsed: true,
            contentPadding: const EdgeInsets.fromLTRB(35, 10, 5, 10)),
        icon: Icon(
          Icons.keyboard_arrow_down,
          color: selectedValue == defaultValue
              ? Colors.grey
              : selectedColor ?? greyDarkColor,
        ),
        iconSize: 20,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w500,
          color: selectedValue == defaultValue
              ? Colors.grey
              : selectedColor ?? greyDarkColor,
        ),
        onChanged: onValueChanged,
        items: itemList.map<DropdownMenuItem<String>>((String e) {
          return DropdownMenuItem<String>(value: e, child: Text(e));
        }).toList(),
        autovalidateMode: AutovalidateMode.onUserInteraction,
      ),
    );
  }
}
// padding: const EdgeInsets.fromLTRB(8, 0, 8, 20),
