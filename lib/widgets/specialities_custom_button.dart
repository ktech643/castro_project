import 'package:castro_app/widgets/primaryColor.dart';
import 'package:flutter/material.dart';

class CustomSubCategoryButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String title;
  final Color bgColor;
  final Color textColor;

  CustomSubCategoryButton({
    required this.onPressed,
    this.title = 'Button',
    this.bgColor = pinkColor,
    this.textColor = Colors.white,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: bgColor,
        ),
        // padding: EdgeInsets.symmetric(
        //   vertical: sizingInformation.safeBlockHorizontal * 2,
        // ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 11.9,
              color: textColor,
            ),
          ),
        ),
      ),
    );
  }
}
