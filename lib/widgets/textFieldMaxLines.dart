import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class TextFieldMaxLines extends StatefulWidget {
  FontWeight fontWeight;
  String? hintText, label;
  TextInputType? keyboardType;
  bool? obscureText;
  TextEditingController textEditingController;
  double padding;
  VoidCallback? onTap;
  bool dateField;
  bool? enable;
  bool isEmail, isFullname;
  String? email, name;
  int maxLines;

  TextFieldMaxLines(this.hintText,
      {Key? key,
      this.padding = 4,
      this.keyboardType,
      this.obscureText = false,
      this.onTap = null,
      required this.textEditingController,
      this.isEmail = false,
      this.isFullname = false,
      this.dateField = false,
      this.label,
      this.enable,
      this.maxLines = 1,
      this.fontWeight = FontWeight.normal})
      : super(key: key);

  @override
  State<TextFieldMaxLines> createState() => _TextFieldMaxLinesState();
}

class _TextFieldMaxLinesState extends State<TextFieldMaxLines> {
  @override
  Widget build(BuildContext context) {
    init(context);

    return Padding(
      padding: EdgeInsets.only(top: height(10)),
      child: TextFormField(
        textAlignVertical: TextAlignVertical.top,
        textInputAction: TextInputAction.next,
        cursorColor: pinkColor,
        enabled: widget.enable,
        controller: widget.textEditingController,
        keyboardType: widget.keyboardType,
        maxLines: widget.maxLines,
        obscureText: widget.obscureText!,
        obscuringCharacter: '*',
        style: GoogleFonts.poppins(
            textStyle: TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: width(16),
          color: blackColor,
        )),
        decoration: InputDecoration(
          filled: true,
          fillColor: whiteColor,
          contentPadding: EdgeInsets.only(left: width(30), top: height(15)),
          labelText: widget.label,
          labelStyle: GoogleFonts.poppins(
              textStyle: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 16,
            color: greyDarkColor,
          )),
          hintText: widget.hintText,
          hintStyle: GoogleFonts.poppins(
              textStyle: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 16,
            color: greyDarkColor,
          )),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: const BorderSide(color: borderColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: const BorderSide(color: pinkColor),
          ),
        ),
      ),
    );
  }
}
