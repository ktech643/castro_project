import 'package:flutter/material.dart';

const Color whiteColor = Color.fromRGBO(255, 255, 255, 1);
const Color scaffoldBgColor = Color.fromRGBO(250, 250, 250, 1);
const Color appBgColor = Color.fromRGBO(229, 229, 229, 1);
const Color orangeLightColor = Color.fromRGBO(255, 251, 243, 1);
const Color greyLightColor = Color.fromRGBO(218, 218, 218, 1);
const Color greyLightColor2 = Color.fromRGBO(243, 244, 249, 1);
const Color greyLightColor3 = Color.fromRGBO(244, 244, 244, 1);
const Color greyLightColor4 = Color.fromRGBO(243, 243, 243, 1);
const Color borderColor = Color.fromRGBO(218, 224, 225, 1);
const Color dividerColor = Color.fromRGBO(210, 210, 210, 1);
const Color greyIconColor = Color.fromRGBO(166, 166, 166, 1);
const Color greyDarkColor0 = Color.fromRGBO(97, 97, 98, 1);
const Color greyDarkColor = Color.fromRGBO(105, 105, 105, 1);
const Color greyDarkColor2 = Color.fromRGBO(109, 109, 109, 1);
const Color greyDarkColor3 = Color.fromRGBO(117, 117, 117, 1);
const Color greyDarkColor3aaa = Color.fromRGBO(117, 117, 117, 1);
const Color greyDarkColor3aa = Color.fromRGBO(125, 125, 125, 1);
const Color greyDarkColor3ab = Color.fromRGBO(135, 135, 135, 1);
const Color greyDarkColor3a = Color.fromRGBO(143, 143, 143, 1);
const Color blackColor4 = Color.fromRGBO(51, 65, 72, 1);
const Color blackColor3 = Color.fromRGBO(41, 45, 50, 1);
const Color blackColor2 = Color.fromRGBO(24, 24, 32, 1);
const Color blackColor = Color.fromRGBO(0, 0, 0, 1);
const Color pinkColor = Color.fromRGBO(253, 130, 150, 1);
const Color pinkColor2 = Color.fromRGBO(254, 166, 180, 1);
const Color pinkLightColor = Color.fromRGBO(253, 130, 150, 0.7);
const Color orangeColor = Color.fromRGBO(255, 159, 124, 1);
const Color orangeColor2 = Color.fromRGBO(201, 73, 210, 1);
const Color purpleColor = Color.fromRGBO(125, 123, 215, 1);
const Color greenColor = Color.fromRGBO(20, 192, 120, 1);
const Color greenColor2 = Color.fromRGBO(47, 209, 112, 1);
const Color yellowColor = Color.fromRGBO(255, 221, 40, 1);
const Color redColor = Color.fromRGBO(242, 46, 46, 1);
