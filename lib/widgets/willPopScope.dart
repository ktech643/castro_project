import 'package:castro_app/widgets/primaryColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

bool willPopScope(BuildContext context, Widget nextScreen) {
  Navigator.pushAndRemoveUntil<dynamic>(
    context,
    MaterialPageRoute<dynamic>(builder: (BuildContext context) => nextScreen),
    (route) => false, //if you want to disable back feature set to false
  );
  return true;
}

showExitAlertDialog(BuildContext context) {
  Widget cancelButton = MaterialButton(
    child: Text("Cancel"),
    onPressed: () {
      Navigator.of(context, rootNavigator: true).pop();
    },
  );
  Widget continueButton = MaterialButton(
    child: Text("Exit", style: TextStyle(color: pinkColor)),
    onPressed: () {
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      // SystemNavigator.pop();
    },
  );
  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Notice!"),
    content: Text('Are you sure you want to exit app?'),
    actions: [
      cancelButton,
      continueButton,
    ],
  );
  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
