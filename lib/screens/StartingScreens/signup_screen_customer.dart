import 'package:bottom_picker/bottom_picker.dart';
import 'package:bottom_picker/resources/arrays.dart';
import 'package:castro_app/screens/StartingScreens/signin_screen.dart';
import 'package:castro_app/services/auth_services.dart';
import 'package:castro_app/utilis/utilis.dart';
import 'package:castro_app/widgets/button.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/textfield_class.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:select_form_field/select_form_field.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/simple_text.dart';

bool isSubmit = false;
bool isSmall = false;
String text = 'Select your date of birth';

class SignupScreenCustomer extends StatefulWidget {
  const SignupScreenCustomer({Key? key}) : super(key: key);

  @override
  _SignupScreenCustomerState createState() => _SignupScreenCustomerState();
}

class _SignupScreenCustomerState extends State<SignupScreenCustomer> {
  final _formKey = GlobalKey<FormState>();
  var finalDate = 'Select your date of birth';
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  String? gender;
  Validator validator = Validator();
  final AuthService authService = AuthService();
  final List<Map<String, dynamic>> _items = [
    {
      'value': 'boxValue',
      'label': 'Male',
    },
    {
      'value': 'circleValue',
      'label': 'Female',
    },
    {
      'value': 'starValue',
      'label': 'Others',
    },
  ];

  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: whiteColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: whiteColor,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.all(20),
            child: SvgPicture.asset(
              'assets/back_button.svg',
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: width(20)),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: height(45)),
                  child: Center(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                color: blackColor2,
                                fontSize: width(32),
                                fontWeight: FontWeight.w700)),
                        children: <TextSpan>[
                          const TextSpan(text: 'Create a'),
                          TextSpan(
                              text: ' Customer',
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(
                                      color: pinkColor,
                                      fontSize: width(32),
                                      fontWeight: FontWeight.w700))),
                          const TextSpan(text: '\nAccount'),
                          // const TextSpan(text: 'km'),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: height(35)),
                TextFieldClass('Name',
                    label: 'Name',
                    textEditingController: nameController,
                    validator: Validator.validateName,
                    keyboardType: TextInputType.name),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass('Email',
                    validator: Validator.validateEmail,
                    label: 'Email',
                    textEditingController: emailController,
                    keyboardType: TextInputType.emailAddress),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass('Password',
                    validator: Validator.validatePassword,
                    label: 'Password',
                    textEditingController: passwordController,
                    obscureText: true),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass('+95532653263',
                    // validator: Validator.validatePhoneNo,
                    label: 'Phone No',
                    textEditingController: phoneNoController,
                    keyboardType: TextInputType.phone),
                SizedBox(
                  height: 20,
                ),
                SelectFormField(
                  type: SelectFormFieldType.dropdown,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: width(16),
                    color: blackColor,
                  )),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: width(30)),
                    labelText: "Select your gender",
                    labelStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      color: greyDarkColor,
                    )),
                    hintText: 'Select your gender',
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      color: greyDarkColor,
                    )),
                    fillColor: Colors.white,
                    focusColor: Colors.white,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: const BorderSide(color: borderColor),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: const BorderSide(color: borderColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: const BorderSide(color: pinkColor),
                    ),
                    suffixIcon: Icon(Icons.arrow_drop_down),
                  ),
                  items: _items,
                  onChanged: (val) {
                    setState(() {
                      gender = val;
                    });
                  },
                  // validator: ((value) {
                  //   if (value!.isEmpty) {
                  //     String message;
                  //     return message = 'Please select gender';
                  //   }
                  // }),
                  onSaved: (val) => print(val),
                ),
                SizedBox(
                  height: 20,
                ),
                _datePicker(),
                SizedBox(height: height(54)),
                Button('Sign up', onTap: () {
                  if (_formKey.currentState!.validate()) {
                    authService.customerSignUp(
                        email: emailController.text,
                        name: nameController.text,
                        phoneNo: phoneNoController.text,
                        password: passwordController.text,
                        dob: finalDate != null ? finalDate : null,
                        gender: gender,
                        context: context);
                  }
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => const BottomNavBar()));
                }),
                SizedBox(height: height(10)),
                Padding(
                  padding: EdgeInsets.only(left: width(7)),
                  child: RichText(
                    text: TextSpan(
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              color: greyDarkColor,
                              fontSize: width(14),
                              fontWeight: FontWeight.w500)),
                      children: <TextSpan>[
                        const TextSpan(text: 'By signing in, I agree to the'),
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                validator.launchPrivacyUrl();
                              },
                            text: ' Castro Privacy Statement',
                            style: GoogleFonts.nunito(
                                textStyle: TextStyle(
                              fontSize: width(14),
                              fontWeight: FontWeight.w300,
                              color: pinkColor,
                            ))),
                        const TextSpan(text: ' and'),
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                validator.launchTermUrl();
                              },
                            text: ' Terms of Service',
                            style: GoogleFonts.nunito(
                                textStyle: const TextStyle(
                              color: pinkColor,
                            ))),
                        const TextSpan(text: '.'),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: height(25)),
                Center(
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      style: GoogleFonts.nunito(
                          textStyle: TextStyle(
                              color: greyDarkColor,
                              fontSize: width(18),
                              fontWeight: FontWeight.w600)),
                      children: <TextSpan>[
                        const TextSpan(text: 'Already have an account?'),
                        TextSpan(
                          text: ' Sign in',
                          style: GoogleFonts.nunito(
                              textStyle: TextStyle(
                                  color: pinkColor,
                                  fontSize: width(18),
                                  fontWeight: FontWeight.w600)),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => const SigninScreen()));
                            },
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: height(20)),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _datePicker() {
    return InkWell(
      onTap: () {
        // openDatePicker(context);
        BottomPicker.date(
          title: 'Select Date',
          titleStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: width(15),
            color: orangeColor,
          ),
          dateOrder: DatePickerDateOrder.dmy,
          pickerTextStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: width(13),
            color: blackColor,
          ),
          onChange: (index) {},
          onSubmit: (index) {
            final dateFormat = DateFormat('yyyy-MM-dd');
            setState(() {
              isSubmit = true;
              // finalDate = dateFormat.format(index);
              finalDate = dateFormat.format(index);
              text = finalDate;
            });
          },
          bottomPickerTheme: BOTTOM_PICKER_THEME.orange,
        ).show(context);
      },
      child: Container(
        height: isSmall ? height(54) : height(49),
        width: double.infinity,
        decoration: BoxDecoration(
            color: isSmall ? greyLightColor3 : whiteColor,
            borderRadius: BorderRadius.circular(width(10)),
            border: Border.all(color: borderColor)),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: width(18)),
          child: Row(
            mainAxisAlignment: isSmall
                ? MainAxisAlignment.spaceEvenly
                : MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(left: width(10)),
                child: SimpleText(text,
                    color: isSmall ? blackColor : greyDarkColor,
                    fontSize: isSmall ? 14 : 16),
              ),
              isSmall
                  ? isSubmit
                      ? SizedBox(height: 5, width: 5)
                      : Icon(Icons.arrow_drop_down)
                  : SvgPicture.asset('assets/calendar2.svg'),
            ],
          ),
        ),
      ),
    );
  }
}
