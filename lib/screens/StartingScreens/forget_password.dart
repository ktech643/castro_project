import 'package:castro_app/services/auth_services.dart';
import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/simple_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../widgets/button.dart';
import '../../widgets/textfield_class.dart';

class ForgetPassword extends StatefulWidget {
  const ForgetPassword({Key? key}) : super(key: key);

  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  TextEditingController emailController = TextEditingController();
  AuthService authService = AuthService();

  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: whiteColor,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.all(20),
            child: SvgPicture.asset(
              'assets/back_button.svg',
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: width(20)),
          child: Center(
            child: Column(
              children: [
                SizedBox(height: height(27)),
                SimpleText('Forgot Password',
                    fontWeight: FontWeight.w700,
                    fontSize: 32,
                    color: blackColor2),
                SizedBox(height: height(51)),
                SvgPicture.asset('assets/forget_password.svg',
                    height: height(296), width: width(223)),
                SizedBox(height: height(19)),
                Align(
                    alignment: Alignment.centerLeft,
                    child: SimpleText('Enter Email',
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: greyDarkColor3)),
                SizedBox(height: height(12)),
                TextFieldClass('abc123@gmail.com',
                    textEditingController: emailController,
                    keyboardType: TextInputType.emailAddress),
                SizedBox(height: height(24)),
                Button('Send OTP', onTap: () {
                  authService.forgetPassword(
                      email: emailController.text, context: context);
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => const OPTScreen()));
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
