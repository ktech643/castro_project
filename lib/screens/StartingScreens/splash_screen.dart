import 'dart:async';

import 'package:castro_app/screens/StartingScreens/signup_selection_screen.dart';
import 'package:castro_app/utilis/secure_storage.dart';
import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/simple_text.dart';
import 'package:flutter/material.dart';

import '../../widgets/button.dart';
import '../core/bottom_navigation_bar.dart';
import '../core/professional_bottom_navigation_bar.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SecureStorage secureStorage = SecureStorage();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _land();
    // secureStorage.readStore("token").then((value) {
    //   print("Its user token ${value}");
    // });
  }

  _land() {
    SecureStorage _secureStorage = SecureStorage();
    _secureStorage.readStore('token').then((token) {
      _secureStorage.readStore('role').then((role) {
        switch (role) {
          case 'customer':
            Timer(
                Duration(seconds: 0),
                () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const BottomNavBar())));
            break;
          case 'professional':
            Timer(
                Duration(seconds: 0),
                () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ProfessionalBottomNavBar())));
            break;
          // default:
          //   Timer(
          //       Duration(seconds: 6),
          //       () => Navigator.pushNamedAndRemoveUntil(
          //           context, loginScreen, (route) => false));
          //   break;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
              color: whiteColor,

              borderRadius: BorderRadius.circular(16),
              image: DecorationImage(
                  image: AssetImage(
                'assets/start.png',
              ))),
          child: Column(
            children: [
              Expanded(child: Container()),
              Expanded(
                child: Stack(
                  children: [

                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          SimpleText(
                              'Find the best beauty professional',
                              textAlign: TextAlign.center,
                              fontWeight: FontWeight.w400,
                              color: Colors.black),
                          SizedBox(height: height(26)),
                          Button(
                            'Get Started',
                            singleColor: true,
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      const SignUpSelectionScreen()));
                            },
                          ),
                          SizedBox(height: height(41)),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
