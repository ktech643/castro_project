import 'package:castro_app/screens/StartingScreens/signin_screen.dart';
import 'package:castro_app/screens/StartingScreens/signup_screen_customer.dart';
import 'package:castro_app/screens/StartingScreens/signup_screen_professional.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../widgets/button.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/simple_text.dart';

class SignUpSelectionScreen extends StatefulWidget {
  const SignUpSelectionScreen({Key? key}) : super(key: key);

  @override
  _SignUpSelectionScreenState createState() => _SignUpSelectionScreenState();
}

class _SignUpSelectionScreenState extends State<SignUpSelectionScreen> {
  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: whiteColor,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.all(20),
            child: SvgPicture.asset(
              'assets/back_button.svg',
            ),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: width(20)),
        child: Column(
          children: [
            SizedBox(height: height(100)),
            SimpleText('Register As',
                fontWeight: FontWeight.w700, fontSize: 32, color: blackColor3),
            SizedBox(height: height(157)),
            Button('Customer', singleColor: true, buttonWithBorder: true,
                onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const SignupScreenCustomer()));
            }),
            SizedBox(height: height(20)),
            Button('Professional', onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const SignupScreenProfessional()));
            }),
            Spacer(),
            Center(
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  style: GoogleFonts.nunito(
                      textStyle: TextStyle(
                          color: greyDarkColor,
                          fontSize: width(18),
                          fontWeight: FontWeight.w600)),
                  children: <TextSpan>[
                    const TextSpan(text: 'Already have an account?'),
                    TextSpan(
                      text: ' Sign in',
                      style: GoogleFonts.nunito(
                          textStyle: TextStyle(
                              color: pinkColor,
                              fontSize: width(18),
                              fontWeight: FontWeight.w600)),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => const SigninScreen()));
                        },
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}
