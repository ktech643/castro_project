import 'package:castro_app/screens/StartingScreens/signup_selection_screen.dart';
import 'package:castro_app/services/auth_services.dart';
import 'package:castro_app/utilis/utilis.dart';
import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/simple_text.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../widgets/button.dart';
import '../../widgets/scalling.dart';
import '../../widgets/textfield_class.dart';
import 'forget_password.dart';

class SigninScreen extends StatefulWidget {
  const SigninScreen({Key? key}) : super(key: key);

  @override
  _SigninScreenState createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool visiblePassword = true;
  AuthService authService = AuthService();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: whiteColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: whiteColor,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.all(20),
            child: SvgPicture.asset(
              'assets/back_button.svg',
            ),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: width(20)),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                SizedBox(height: height(18)),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: height(53)),
                  child: SimpleText('Log in',
                      color: blackColor2,
                      fontWeight: FontWeight.w700,
                      fontSize: 32),
                ),
                SizedBox(height: height(50)),
                TextFieldClass('Email',
                    label: 'Email',
                    validator: Validator.validateEmail,
                    textEditingController: emailController,
                    keyboardType: TextInputType.emailAddress),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass('Password',
                    label: 'Password',
                    validator: Validator.validatePassword,
                    suffix: InkWell(
                      onTap: () =>
                          setState(() => visiblePassword = !visiblePassword),
                      child: visiblePassword
                          ? Icon(
                              Icons.visibility_off,
                              color: Colors.grey,
                            )
                          : Icon(Icons.visibility, color: Colors.pink),
                    ),
                    textEditingController: passwordController,
                    obscureText: visiblePassword),
                Padding(
                  padding: EdgeInsets.only(top: height(10)),
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => const ForgetPassword()));
                          },
                          child: SimpleText('Forget password ?',
                              color: greyDarkColor,
                              nunito: true,
                              fontSize: 18,
                              fontWeight: FontWeight.w600))),
                ),
                SizedBox(height: height(34)),
                Button(
                  'Sign in',
                  onTap: () {
                    if (_formKey.currentState!.validate()) {
                      authService.signIn(
                          email: emailController.text,
                          context: context,
                          password: passwordController.text);
                    }
                  },
                ),
                SizedBox(height: height(15)),
                // Padding(
                //   padding: EdgeInsets.symmetric(vertical: height(20)),
                //   child: SimpleText('or', color: greyDarkColor),
                // ),
                // Container(
                //     height: height(49),
                //     decoration: BoxDecoration(
                //       borderRadius: BorderRadius.circular(10.0),
                //       border: Border.all(color: borderColor),
                //     ),
                //     child: ListTile(
                //       dense: true,
                //       contentPadding: EdgeInsets.only(left: width(25)),
                //       leading: SvgPicture.asset('assets/facebook.svg'),
                //       title: SimpleText('Continue with Facebook',
                //           color: greyDarkColor),
                //     )),
                SizedBox(height: height(77)),
                Center(
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      style: GoogleFonts.nunito(
                          textStyle: TextStyle(
                              color: greyDarkColor,
                              fontSize: width(18),
                              fontWeight: FontWeight.w600)),
                      children: <TextSpan>[
                        const TextSpan(text: 'Don’t have an account?'),
                        TextSpan(
                          text: ' Sign up',
                          style: GoogleFonts.nunito(
                              textStyle: TextStyle(
                                  color: pinkColor,
                                  fontSize: width(18),
                                  fontWeight: FontWeight.w600)),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      const SignUpSelectionScreen()));
                            },
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: height(20)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
