// import 'dart:async';
// import 'package:castro_app/screens/StartingScreens/signin_screen.dart';
// import 'package:castro_app/services/auth_services.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:pin_code_fields/pin_code_fields.dart';
//
// import '../../widgets/button.dart';
// import '../../widgets/primaryColor.dart';
// import '../../widgets/scalling.dart';
// import '../../widgets/simple_text.dart';
//
// class OPTScreen extends StatefulWidget {
//   final String email;
//
//   OPTScreen({required this.email});
//
//   @override
//   _OPTScreenState createState() => _OPTScreenState();
// }
//
// class _OPTScreenState extends State<OPTScreen> {
//   final formKey = GlobalKey<FormState>();
//   TextEditingController textEditingController = TextEditingController();
//   StreamController<ErrorAnimationType>? errorController;
//   String currentText = "";
//   AuthService authService = AuthService();
//
//   @override
//   void initState() {
//     errorController = StreamController<ErrorAnimationType>();
//     super.initState();
//   }
//
//   @override
//   void dispose() {
//     errorController!.close();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     init(context);
//     return Scaffold(
//       backgroundColor: whiteColor,
//       appBar: AppBar(
//         elevation: 0,
//         backgroundColor: whiteColor,
//         leading: InkWell(
//           onTap: () {
//             Navigator.pop(context);
//           },
//           child: Padding(
//             padding: EdgeInsets.all(20),
//             child: SvgPicture.asset(
//               'assets/back_button.svg',
//             ),
//           ),
//         ),
//       ),
//       body: Padding(
//         padding: EdgeInsets.symmetric(horizontal: width(20)),
//         child: Center(
//           child: Column(
//             // mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               SizedBox(height: height(140)),
//               SimpleText('Enter OTP',
//                   fontWeight: FontWeight.w700,
//                   fontSize: 32,
//                   color: blackColor2),
//               SizedBox(height: height(20)),
//               SimpleText(
//                   'Enter the 4 digits code that was sent \nto your email.',
//                   fontWeight: FontWeight.w400,
//                   fontSize: 14,
//                   color: greyDarkColor,
//                   textAlign: TextAlign.center),
//               SizedBox(height: height(41)),
//               Form(
//                 key: formKey,
//                 child: Padding(
//                     padding: const EdgeInsets.symmetric(horizontal: 30),
//                     child: PinCodeTextField(
//                       appContext: context,
//                       pastedTextStyle: const TextStyle(
//                         color: pinkColor,
//                         fontWeight: FontWeight.bold,
//                       ),
//                       length: 4,
//                       blinkWhenObscuring: true,
//                       animationType: AnimationType.fade,
//                       hintCharacter: '0',
//                       validator: (v) {
//                         if (v!.length < 3) {
//                           return "Enter 4 digit code";
//                         } else {
//                           return null;
//                         }
//                       },
//                       pinTheme: PinTheme(
//                         shape: PinCodeFieldShape.box,
//                         borderRadius: BorderRadius.circular(width(10)),
//                         fieldHeight: height(54),
//                         fieldWidth: width(58),
//                         activeFillColor: whiteColor,
//                         inactiveFillColor: whiteColor,
//                         activeColor: pinkColor,
//                         inactiveColor: pinkColor,
//                         selectedColor: pinkColor,
//                         selectedFillColor: pinkColor,
//                       ),
//                       cursorColor: blackColor,
//                       animationDuration: const Duration(milliseconds: 300),
//                       enableActiveFill: true,
//                       errorAnimationController: errorController,
//                       controller: textEditingController,
//                       keyboardType: TextInputType.number,
//                       boxShadows: const [
//                         BoxShadow(
//                           offset: Offset(0, 1),
//                           color: Colors.black12,
//                           blurRadius: 10,
//                         )
//                       ],
//                       onCompleted: (v) {
//                       },
//                       onChanged: (value) {
//                         print(value);
//                         setState(() {
//                           currentText = value;
//                         });
//                       },
//                       beforeTextPaste: (text) {
//                         return true;
//                       },
//                     )),
//               ),
//               SizedBox(height: height(15)),
//               Button('Submit', onTap: () {
//                 authService.sendOTP(
//                     email: widget.email,
//                     otp: textEditingController.text,
//                     context: context);
//                 // Navigator.of(context).push(MaterialPageRoute(
//                 //     builder: (context) => const SigninScreen()));
//               }),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
