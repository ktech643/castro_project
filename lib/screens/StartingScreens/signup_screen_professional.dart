import 'dart:async';
import 'package:castro_app/models/professional/specialities_list.dart';
import 'package:castro_app/screens/StartingScreens/signin_screen.dart';
import 'package:castro_app/services/auth_services.dart';
import 'package:castro_app/services/professional_services.dart';
import 'package:castro_app/utilis/utilis.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/textFieldMaxLines.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:select_form_field/select_form_field.dart';
import '../../widgets/button.dart';
import '../../widgets/custom_dropDown.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/textfield_class.dart';
import 'package:geocoding/geocoding.dart';

class SignupScreenProfessional extends StatefulWidget {
  const SignupScreenProfessional({Key? key}) : super(key: key);

  @override
  _SignupScreenProfessionalState createState() =>
      _SignupScreenProfessionalState();
}

class _SignupScreenProfessionalState extends State<SignupScreenProfessional> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  TextEditingController experienceController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController zipCodeController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController searchMapController = TextEditingController();
  String? speciality;
  AuthService authService = AuthService();
  List<SpecialtiesModel> specialitiesModel = [];
  String _selectSpeciality = "Select your speciality";
  Completer<GoogleMapController> _mapController = Completer();
  GoogleMapController? _googleMapController;
  String? _mapStyle;
  Validator validator = Validator();

  // LatLng dummyPostion = LatLng(42.448680, -83.459900);
  LatLng currentPostion = LatLng(37.0902, 95.7129);
  final List<Map<String, dynamic>> _items = [
    {
      'value': 'boxValue',
      'label': 'Esthetician',
    },
    {
      'value': 'circleValue',
      'label': 'Nails Specialist',
    },
    {
      'value': 'starValue',
      'label': 'Cosmetologist',
    },
    {
      'value': 'newValue',
      'label': 'Natural Hair Stylist',
    },
    {
      'value': 'barberValue',
      'label': 'Barber',
    },
  ];
  ProfessionalServices professionalServices = ProfessionalServices();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: whiteColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: whiteColor,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.all(20),
            child: SvgPicture.asset(
              'assets/back_button.svg',
            ),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: width(20)),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: height(45)),
                  child: Center(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                color: blackColor2,
                                fontSize: width(30),
                                fontWeight: FontWeight.w700)),
                        children: <TextSpan>[
                          const TextSpan(text: 'Create a'),
                          TextSpan(
                              text: ' Professional',
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(
                                      color: pinkColor,
                                      fontSize: width(30),
                                      fontWeight: FontWeight.w700))),
                          const TextSpan(text: '\nAccount'),
                          // const TextSpan(text: 'km'),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: height(35)),
                TextFieldClass('Name',
                    label: 'Name',
                    textEditingController: nameController,
                    validator: Validator.validateName,
                    keyboardType: TextInputType.name),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass('Email',
                    textEditingController: emailController,
                    validator: Validator.validateEmail,
                    label: 'Email',
                    keyboardType: TextInputType.emailAddress),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass('Password',
                    validator: Validator.validatePassword,
                    textEditingController: passwordController,
                    label: 'Password',
                    obscureText: true),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass('+95532653263',
                    textEditingController: phoneNoController,
                    label: 'Phone No',
                    keyboardType: TextInputType.phone),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass('Zip Code',
                    textEditingController: zipCodeController,
                    label: 'Zip Code',
                    keyboardType: TextInputType.phone),
                SizedBox(
                  height: 20,
                ),
                TextFieldMaxLines('Enter your address',
                    textEditingController: addressController,
                    label: 'Enter your address',
                    keyboardType: TextInputType.multiline,
                    maxLines: 5),
                SizedBox(
                  height: 20,
                ),
                // Padding(
                //   padding: EdgeInsets.only(top: height(10)),
                //   child: Container(
                //     height: height(49),
                //     width: double.infinity,
                //     child: FutureBuilder(
                //       future: professionalServices.getSpecialities(),
                //       builder: (context, snapshot) {
                //         if (snapshot.hasData) {
                //           specialitiesModel =
                //               snapshot.data as List<SpecialtiesModel>;
                //           List<Map<String, dynamic>> specialitiesName =
                //               specialitiesModel
                //                   .map((s) => {'name': s.name, 'id': s.id})
                //                   .toList();
                //           print("$specialitiesName++++++++++++++++++++");
                //           return SelectFormField(
                //             type: SelectFormFieldType.dropdown,
                //             style: GoogleFonts.poppins(
                //                 textStyle: TextStyle(
                //               fontWeight: FontWeight.w500,
                //               fontSize: width(16),
                //               color: blackColor,
                //             )),
                //             initialValue: "Something",
                //             decoration: InputDecoration(
                //               contentPadding: EdgeInsets.only(left: width(30)),
                //               labelText: "Select your speciality",
                //               labelStyle: GoogleFonts.poppins(
                //                   textStyle: TextStyle(
                //                 fontWeight: FontWeight.w500,
                //                 fontSize: 16,
                //                 color: greyDarkColor,
                //               )),
                //               hintText: 'Select your speciality',
                //               hintStyle: GoogleFonts.poppins(
                //                   textStyle: TextStyle(
                //                 fontWeight: FontWeight.w500,
                //                 fontSize: 16,
                //                 color: greyDarkColor,
                //               )),
                //               fillColor: Colors.white,
                //               focusColor: Colors.white,
                //               enabledBorder: OutlineInputBorder(
                //                 borderRadius: BorderRadius.circular(10.0),
                //                 borderSide:
                //                     const BorderSide(color: borderColor),
                //               ),
                //               focusedBorder: OutlineInputBorder(
                //                 borderRadius: BorderRadius.circular(10.0),
                //                 borderSide: const BorderSide(color: pinkColor),
                //               ),
                //               suffixIcon: Icon(Icons.arrow_drop_down),
                //             ),
                //             items: specialitiesName,
                //             onChanged: (val) {
                //               setState(() {
                //                 speciality = val;
                //               });
                //             },
                //             onSaved: (val) => print(val),
                //           );
                //         }
                //         return Container();
                //       },
                //     ),
                //   ),
                // ),
                FutureBuilder(
                  future: professionalServices.getSpecialities(addlist: true),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      specialitiesModel =
                          snapshot.data as List<SpecialtiesModel>;
                      List<String> languageNames = specialitiesModel
                          .map<String>((languageModel) => languageModel.name)
                          .toList();
                      return CustomDropdownWidget(
                        defaultValue: "Select your speciality",
                        selectedValue: _selectSpeciality,
                        itemList: languageNames,
                        onValueChanged: (newValue) {
                          setState(() {
                            _selectSpeciality = newValue!;
                          });
                        },
                        validator: (value) {
                          if (value == "Select your speciality") {
                            return 'Please select speciality';
                          }
                          if (value == null) {
                            return 'Please select speciality';
                          }
                        },
                      );
                    }

                    return Container();
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass('0 year',
                    textEditingController: experienceController,
                    label: 'Experience',
                    keyboardType: TextInputType.number ),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass('00.00',
                    textEditingController: priceController,
                    label: 'Price',
                    keyboardType: TextInputType.text),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass(
                  "Warner Robins, GA, USA",
                  label: "Search map for location",
                  onFieldSubmitted: (_) async {
                    print("WOrking fine");
                    List<Location> locations =
                        await locationFromAddress(searchMapController.text);
                    Location location = locations.first;
                    if (location != null) {
                      setState(() {
                        currentPostion =
                            LatLng(location.latitude, location.longitude);
                        _googleMapController!.moveCamera(
                            CameraUpdate.newCameraPosition(
                                CameraPosition(target: currentPostion, zoom: 14)
                                //17 is new zoom level
                                ));
                      });
                    }
                  },
                  textEditingController: searchMapController,
                ),
                SizedBox(
                  height: 20,
                ),
                Column(
                  children: [
                    SizedBox(
                      height: 300,
                      child: Stack(
                        children: [
                          Material(
                              child: GoogleMap(
                            mapType: MapType.normal,
                            initialCameraPosition: CameraPosition(
                              target: currentPostion,
                              zoom: 14,
                            ),
                            onMapCreated: (GoogleMapController controller) => {
                              setState(() {
                                _googleMapController = controller;
                                _mapController.complete(controller);
                                _googleMapController?.setMapStyle(_mapStyle);
                              })
                            },
                            onCameraMove: ((CameraPosition cp) async {
                              currentPostion = cp.target;
                            }),
                            gestureRecognizers:
                                <Factory<OneSequenceGestureRecognizer>>[
                              new Factory<OneSequenceGestureRecognizer>(
                                () => new EagerGestureRecognizer(),
                              ),
                            ].toSet(),
                            cameraTargetBounds: CameraTargetBounds.unbounded,
                            // markers: Set<Marker>.of(markers.values),
                          )
                              //     :
                              //     GoogleMap(
                              //   mapType: MapType.normal,
                              //   initialCameraPosition: CameraPosition(
                              //     target: dummyPostion,
                              //     zoom: 14,
                              //   ),
                              //   onMapCreated: (GoogleMapController controller) => {
                              //     setState(() {
                              //       _googleMapController = controller;
                              //       _mapController.complete(controller);
                              //       _googleMapController?.setMapStyle(_mapStyle);
                              //     })
                              //   },
                              //   onCameraMove: ((CameraPosition cp) async {
                              //     currentPostion = cp.target;
                              //   }),
                              //   gestureRecognizers:
                              //       <Factory<OneSequenceGestureRecognizer>>[
                              //     new Factory<OneSequenceGestureRecognizer>(
                              //       () => new EagerGestureRecognizer(),
                              //     ),
                              //   ].toSet(),
                              //   cameraTargetBounds: CameraTargetBounds.unbounded,
                              //   // markers: Set<Marker>.of(markers.values),
                              // ),
                              ),
                          Center(
                            child: Icon(
                              Icons.location_on,
                              color: Colors.red,
                              size: 40,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: height(54)),

                Button('Sign up', onTap: () {
                  if (_formKey.currentState!.validate()) {
                    authService.professionalSignUp(
                        email: emailController.text,
                        experience: experienceController.text,
                        price: priceController.text,
                        name: nameController.text,
                        password: passwordController.text,
                        zipcode: zipCodeController.text,
                        address: addressController.text,
                        phoneNo: phoneNoController.text,
                        speciality: specialitiesModel
                            .where(
                                (element) => element.name == _selectSpeciality)
                            .first
                            .id
                            .toString(),
                        latitude: currentPostion.latitude.toString(),
                        longitude: currentPostion.longitude.toString(),
                        context: context);
                  }
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => ProfessionalBottomNavBar()));
                }),
                SizedBox(height: height(10)),
                Padding(
                  padding: EdgeInsets.only(left: width(7)),
                  child: RichText(
                    text: TextSpan(
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              color: greyDarkColor,
                              fontSize: width(14),
                              fontWeight: FontWeight.w500)),
                      children: <TextSpan>[
                        const TextSpan(text: 'By signing in, I agree to the'),
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                validator.launchPrivacyUrl();
                              },
                            text: ' Castro Privacy Statement',
                            style: GoogleFonts.nunito(
                                textStyle: TextStyle(
                              fontSize: width(14),
                              fontWeight: FontWeight.w300,
                              color: pinkColor,
                            ))),
                        const TextSpan(text: ' and'),
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                validator.launchTermUrl();
                              },
                            text: ' Terms of Service',
                            style: GoogleFonts.nunito(
                                textStyle: const TextStyle(
                              color: pinkColor,
                            ))),
                        const TextSpan(text: '.'),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: height(25)),
                Center(
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      style: GoogleFonts.nunito(
                          textStyle: TextStyle(
                              color: greyDarkColor,
                              fontSize: width(18),
                              fontWeight: FontWeight.w600)),
                      children: <TextSpan>[
                        const TextSpan(text: 'Already have an account?'),
                        TextSpan(
                          text: ' Sign in',
                          style: GoogleFonts.nunito(
                              textStyle: TextStyle(
                                  color: pinkColor,
                                  fontSize: width(18),
                                  fontWeight: FontWeight.w600)),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => const SigninScreen()));
                            },
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: height(20)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
