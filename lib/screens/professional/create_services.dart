import 'package:castro_app/services/professional_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../widgets/button.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/scalling.dart';
import '../../widgets/textfield_class.dart';

class CreateServices extends StatefulWidget {
  const CreateServices({Key? key}) : super(key: key);

  @override
  _CreateServicesState createState() => _CreateServicesState();
}

class _CreateServicesState extends State<CreateServices> {
  TextEditingController title = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController price = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  ProfessionalServices professionalServices = ProfessionalServices();

  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: whiteColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: whiteColor,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.all(20),
            child: SvgPicture.asset(
              'assets/back_button.svg',
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: width(20)),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: height(45)),
                  child: Center(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                                color: blackColor2,
                                fontSize: width(32),
                                fontWeight: FontWeight.w700)),
                        children: <TextSpan>[
                          const TextSpan(text: 'Create Service'),
                          // const TextSpan(text: 'km'),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: height(35)),
                TextFieldClass('Title',
                    label: 'Title',
                    textEditingController: title,
                    keyboardType: TextInputType.text),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass('Description',
                    label: 'Description',
                    maxLength: 100,
                    textEditingController: description,
                    keyboardType: TextInputType.text),
                SizedBox(
                  height: 20,
                ),
                TextFieldClass(
                  'Price',
                  label: 'Price',
                  textEditingController: price,
                  keyboardType: TextInputType.number,
                ),
                SizedBox(
                  height: 20,
                ),
                Button('Create', onTap: () {
                  if (_formKey.currentState!.validate()) {
                    professionalServices.addSubServices(
                        name: title.text,
                        description: description.text,
                        price: price.text,
                        context: context);
                    // authService.customerSignUp(
                    //     email: emailController.text,
                    //     name: nameController.text,
                    //     phoneNo: phoneNoController.text,
                    //     password: passwordController.text,
                    //     dob: finalDate != null ? finalDate : null,
                    //     gender: gender,
                    //     context: context);
                  }
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => const BottomNavBar()));
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
