import 'package:castro_app/screens/core/professional_bottom_navigation_bar.dart';
import 'package:castro_app/services/professional_services.dart';
import 'package:castro_app/utilis/constants.dart';
import 'package:castro_app/utilis/secure_storage.dart';
import 'package:castro_app/widgets/button.dart';
import 'package:castro_app/widgets/willPopScope.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import '../../models/professional/appointment_model.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/scalling.dart';
import '../../widgets/simple_text.dart';

class ProfessionalHomeScreen extends StatefulWidget {
  const ProfessionalHomeScreen({Key? key}) : super(key: key);

  @override
  _ProfessionalHomeScreenState createState() => _ProfessionalHomeScreenState();
}

class _ProfessionalHomeScreenState extends State<ProfessionalHomeScreen> {
  ProfessionalServices professionalServices = ProfessionalServices();
  List<AppointmentModel> appointmentList = List.empty(growable: true);
  SecureStorage secureStorage = SecureStorage();
  String? price;
  var temp;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print("Message DAta :${message.data}");
      print("Message Notification :${message.notification}");
      print(message.data);
      if (mounted) {
        getNotification(message);
      }
    });
    readPrice();
  }

  void getNotification(RemoteMessage message) async {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => Dialog(
        child: Container(
          height: 200,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Text(
                message.data['title'],
                style: TextStyle(
                    color: Colors.black,
                    fontFamily: "Segoe UI",
                    fontWeight: FontWeight.w800,
                    fontSize: 16),
              ),
              SizedBox(
                height: 40,
              ),
              Text.rich(
                TextSpan(
                  children: [
                    TextSpan(
                      text: message.data['body'],
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(right: 40, bottom: 20),
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Ok",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: Colors.pink,
                          fontFamily: "Segoe UI"),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }

  readPrice() {
    secureStorage.readStore("price").then((value) {
      setState(() {
        price = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    init(context);
    return WillPopScope(
      onWillPop: () async {
        return showExitAlertDialog(context);
      },
      child: Scaffold(
        backgroundColor: scaffoldBgColor,
        appBar: AppBar(
          backgroundColor: whiteColor,
          centerTitle: true,
          title: SimpleText('My Appointments', color: blackColor3),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: FutureBuilder(
                future: professionalServices.getAppoinments(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    appointmentList = snapshot.data as List<AppointmentModel>;
                    print(appointmentList.length);

                    return appointmentList.isEmpty
                        ? Center(
                            child: Text("No appointment found"),
                          )
                        : ListView.separated(
                            itemCount: appointmentList.length,
                            separatorBuilder:
                                (BuildContext context, int index) =>
                                    SizedBox(height: height(16)),
                            itemBuilder: (BuildContext context, int index) {
                              temp = appointmentList[index]
                                  .booking_date!
                                  .split(" ");
                              return InkWell(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) =>
                                          ProfessionalBottomNavBar(
                                              appointmentModel:
                                                  appointmentList[index],
                                              selectedIndex: 2)));
                                },
                                child: Column(
                                  children: [
                                    if (index == 0)
                                      SizedBox(height: height(10)),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: width(21)),
                                      child: Card(
                                        color: whiteColor,
                                        elevation: 3,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(width(10)),
                                        ),
                                        child: Container(
                                          height: height(122),
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: width(15)),
                                            child: Center(
                                              child: Container(
                                                height: height(92),
                                                child: Row(
                                                  children: [
                                                    appointmentList[index]
                                                                .customer_user!
                                                                .image !=
                                                            null
                                                        ? Container(
                                                            height:
                                                                height(89.06),
                                                            width: width(85.76),
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(width(
                                                                            10)),
                                                                color:
                                                                    whiteColor,
                                                                image: DecorationImage(
                                                                    image: NetworkImage(Constants
                                                                            .IMAGE_URL +
                                                                        appointmentList[index]
                                                                            .customer_user!
                                                                            .image
                                                                            .toString()),
                                                                    fit: BoxFit
                                                                        .fill)),
                                                          )
                                                        : Container(
                                                            height:
                                                                height(89.06),
                                                            width: width(85.76),
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(width(
                                                                            10)),
                                                                color:
                                                                    whiteColor,
                                                                image: const DecorationImage(
                                                                    image: AssetImage(
                                                                        "assets/profile.png"),
                                                                    fit: BoxFit
                                                                        .fill)),
                                                          ),
                                                    Expanded(
                                                      child: Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left:
                                                                    width(10)),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                SimpleText(
                                                                  appointmentList[
                                                                          index]
                                                                      .customer_user!
                                                                      .name,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600,
                                                                  fontSize: 14,
                                                                  color:
                                                                      blackColor3,
                                                                ),
                                                                SimpleText(
                                                                  appointmentList[index]
                                                                              .total_amount !=
                                                                          null
                                                                      ? '\$${appointmentList[index].total_amount}.00'
                                                                      : "\$ 0.00",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600,
                                                                  fontSize: 14,
                                                                  color:
                                                                      pinkColor,
                                                                ),
                                                              ],
                                                            ),
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                SimpleText(
                                                                  temp[0],
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                  fontSize: 10,
                                                                  color:
                                                                      pinkColor,
                                                                ),
                                                                // SimpleText(
                                                                //   appointmentList[
                                                                //           index]
                                                                //       .booking_time,
                                                                //   fontWeight:
                                                                //       FontWeight
                                                                //           .w500,
                                                                //   fontSize: 10,
                                                                //   color:
                                                                //       pinkColor,
                                                                // ),
                                                              ],
                                                            ),
                                                            appointmentList[index]
                                                                        .status ==
                                                                    "in-process"
                                                                ? Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceBetween,
                                                                    children: [
                                                                      SmallButton(
                                                                        'Accept',
                                                                        onTap:
                                                                            () {
                                                                          professionalServices.acceptAppointment(
                                                                              book_specialist_id: appointmentList[index].id,
                                                                              context: context);
                                                                        },
                                                                      ),
                                                                      SmallButton(
                                                                          'Decline',
                                                                          onTap:
                                                                              () {
                                                                        professionalServices.rejectAppointment(
                                                                            book_specialist_id:
                                                                                appointmentList[index].id,
                                                                            context: context);
                                                                      },
                                                                          isAccept:
                                                                              false),
                                                                    ],
                                                                  )
                                                                : Container(
                                                                    height: height(
                                                                        29.31),
                                                                    width: width(
                                                                        90.8),
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      color:
                                                                          pinkColor2,
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              width(5)),
                                                                    ),
                                                                    child:
                                                                        Center(
                                                                      child:
                                                                          SimpleText(
                                                                        appointmentList[index]
                                                                            .status
                                                                            .toString(),
                                                                        fontSize:
                                                                            10,
                                                                        color:
                                                                            whiteColor,
                                                                      ),
                                                                    ),
                                                                  )
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    if (index == 9)
                                      SizedBox(height: height(20)),
                                  ],
                                ),
                              );
                            },
                          );
                  }
                  return const Center(
                    child: Text("No appointment found"),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
