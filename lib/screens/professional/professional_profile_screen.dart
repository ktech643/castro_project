import 'dart:io';
import 'package:castro_app/screens/core/professional_bottom_navigation_bar.dart';
import 'package:castro_app/screens/professional/sub_services_secreen.dart';
import 'package:castro_app/services/professional_services.dart';
import 'package:castro_app/utilis/secure_storage.dart';
import 'package:castro_app/widgets/textFieldMaxLines.dart';
import 'package:castro_app/widgets/willPopScope.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/textfield_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import '../../models/professional/professional_user_model.dart';
import '../../models/professional/specialities_list.dart';
import '../../services/customer_services.dart';
import '../../utilis/constants.dart';
import '../../widgets/button.dart';
import '../../widgets/custom_dropDown.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/simple_text.dart';
import '../StartingScreens/signin_screen.dart';

class ProfessionalProfileScreen extends StatefulWidget {
  const ProfessionalProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfessionalProfileScreenState createState() =>
      _ProfessionalProfileScreenState();
}

class _ProfessionalProfileScreenState extends State<ProfessionalProfileScreen> {
  XFile? photo;
  final ImagePicker _picker = ImagePicker();
  String? profileImage;
  String? name;
  String? email;
  TextEditingController addressController = TextEditingController();
  SecureStorage secureStorage = SecureStorage();
  bool isLoading = false;
  CustomerServices customerServices = CustomerServices();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController zipController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController experienceController = TextEditingController();
  ProfessionalServices professionalServices = ProfessionalServices();
  ProfessionalUserModel? professionalUserModel;
  List<SpecialtiesModel> specialitiesModel = [];
  String _selectSpeciality = "Select your speciality";
  bool enableEdit = false;
  bool imageLoading = false;

  Future<void> getImageCameraAndImage(bool isCamera) async {
    Navigator.of(context).pop();
    photo = await _picker.pickImage(
        source: isCamera == true ? ImageSource.camera : ImageSource.gallery);

    if (photo != null) {
      setState(() {
        profileImage = photo!.path;
      });
    }
  }

  _assignValue() {
    print(" SPEciality ID${professionalUserModel!.speciality_id}");
    if (professionalUserModel!.name != null) {
      nameController.text = professionalUserModel!.name!;
    }
    if (professionalUserModel!.email != null) {
      emailController.text = professionalUserModel!.email!;
    }
    if (professionalUserModel!.phone != null) {
      phoneController.text = professionalUserModel!.phone!;
    }
    if (professionalUserModel!.price != null) {
      secureStorage.setStore("price", professionalUserModel!.price);
      priceController.text = professionalUserModel!.price!;
    }
    if (professionalUserModel!.experience != null) {
      experienceController.text = professionalUserModel!.experience!;
    }
    if (professionalUserModel!.address != null) {
      addressController.text = professionalUserModel!.address!;
    }
    if (professionalUserModel!.zip_code != null) {
      zipController.text = professionalUserModel!.zip_code!;
    }
  }

  void showCameraGalleryAlert(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text(
              'Choose option',
              style:
                  TextStyle(fontWeight: FontWeight.w600, color: Colors.black),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  InkWell(
                    onTap: () {
                      getImageCameraAndImage(true);
                    },
                    splashColor: Colors.purpleAccent,
                    child: Row(
                      children: const [
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.camera,
                            color: pinkColor,
                          ),
                        ),
                        Text(
                          'Camera',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: pinkColor),
                        )
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      getImageCameraAndImage(false);
                    },
                    splashColor: Colors.purpleAccent,
                    child: Row(
                      children: const [
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.image,
                            color: pinkColor,
                          ),
                        ),
                        Text(
                          'Gallery',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: pinkColor),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  final List<Map<String, dynamic>> _items = [
    {
      'value': 'boxValue',
      'label': 'Esthetician',
    },
    {
      'value': 'circleValue',
      'label': 'Nails Specialist',
    },
    {
      'value': 'starValue',
      'label': 'Cosmetologist',
    },
    {
      'value': 'newValue',
      'label': 'Natural Hair Stylist',
    },
    {
      'value': 'barberValue',
      'label': 'Barber',
    },
  ];

  final List<Map<String, dynamic>> _experience = [
    {
      'value': 'one',
      'label': '1',
    },
    {
      'value': 'two',
      'label': '2',
    },
    {
      'value': 'three',
      'label': '3',
    },
    {
      'value': 'four',
      'label': '4',
    },
    {
      'value': 'five',
      'label': '5',
    },
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    customerServices.getUserProfile().then((value) {
      if (value != null) {
        setState(() {
          professionalUserModel = value;
          imageLoading = true;
          // print(professionalUserModel!.name);
          print("IN INIT");
          _assignValue();
          professionalServices.getSpecialities(addlist: true).then((value) {
            specialitiesModel = value;
            print(specialitiesModel.length);
            if (professionalUserModel!.speciality_id != null) {
              _selectSpeciality = specialitiesModel
                  .where((element) =>
                      element.id == professionalUserModel!.speciality_id)
                  .first
                  .name;
            }
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    init(context);
    return WillPopScope(
      onWillPop: () async {
        return willPopScope(context, ProfessionalBottomNavBar());
      },
      child: Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          elevation: 5,
          backgroundColor: whiteColor,
          centerTitle: true,
          actions: [
            IconButton(
                onPressed: () {
                  secureStorage.deleteAll();
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => SigninScreen()),
                      (Route<dynamic> route) => false);
                },
                icon: Icon(
                  Icons.power_settings_new,
                  color: Colors.black,
                ))
          ],
          // leading: InkWell(
          //   onTap: () {
          //     Navigator.pop(context);
          //   },
          //   child: Padding(
          //     padding: EdgeInsets.all(20),
          //     child: SvgPicture.asset(
          //       'assets/back_button.svg',
          //     ),
          //   ),
          // ),
          title: SimpleText('My Profile',
              fontWeight: FontWeight.w600, fontSize: 18, color: blackColor),
        ),
        body: Column(
          children: [
            SizedBox(height: height(40)),
            Container(
              height: height(93),
              width: width(93),
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [
                    orangeColor2,
                    pinkColor,
                    orangeColor,
                  ],
                ),
              ),
              child: Stack(
                children: [
                  imageLoading
                      ? Center(
                          child: Container(
                            height: height(85),
                            width: width(85),
                            decoration: profileImage == null
                                ? professionalUserModel!.image == null
                                    ? BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: AssetImage(
                                              'assets/profile.png',
                                            )))
                                    : BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: NetworkImage(
                                              Constants.IMAGE_URL +
                                                  professionalUserModel!.image
                                                      .toString(),
                                            )))
                                : BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: FileImage(File(profileImage!)))),
                          ),
                        )
                      : SizedBox(),
                  InkWell(
                    onTap: () {
                      if (enableEdit == false) {
                        toast(message: "Enable editing option first.");
                      } else {
                        showCameraGalleryAlert(context);
                      }
                    },
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        height: height(30),
                        width: width(30),
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.3),
                              spreadRadius: 3,
                              blurRadius: 4,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ],
                          shape: BoxShape.circle,
                          color: whiteColor,
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(width(6)),
                          child: SvgPicture.asset('assets/edit_icon2.svg'),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: height(20)),
            Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SubServicesSecreen()));
                    },
                    child: Text(
                      "Go to services",
                      style: TextStyle(
                          color: pinkColor,
                          decoration: TextDecoration.underline),
                    ),
                  ),
                )),
            SizedBox(height: height(10)),
            Expanded(
                child: Container(
              decoration: BoxDecoration(
                  color: greyLightColor3,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(width(25)),
                      topLeft: Radius.circular(width(25)))),
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width(20)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: height(27)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(),
                          SimpleText('Details'),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                enableEdit = true;
                                toast(message: "Editing enabled");
                              });
                            },
                            child: SvgPicture.asset(
                              'assets/edit_icon.svg',
                              fit: BoxFit.fill,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: height(10.4)),
                      SimpleText('Name',
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: greyDarkColor),
                      TextFieldClass('Enter your name',
                          textEditingController: nameController,
                          enable: enableEdit,
                          keyboardType: TextInputType.emailAddress),
                      SizedBox(
                        height: 20,
                      ),
                      SimpleText('Email',
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: greyDarkColor),
                      TextFieldClass('Enter your email',
                          textEditingController: emailController,
                          enable: false,
                          fillColor: Colors.grey.withOpacity(0.2),
                          keyboardType: TextInputType.emailAddress),
                      Padding(
                        padding: EdgeInsets.only(top: height(20)),
                        child: SimpleText('Phone Number',
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: greyDarkColor),
                      ),
                      TextFieldClass('Enter you phoneNo',
                          textEditingController: phoneController,
                          enable: enableEdit,
                          keyboardType: TextInputType.phone),
                      Padding(
                        padding: EdgeInsets.only(top: height(20)),
                        child: SimpleText('Zip Code',
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: greyDarkColor),
                      ),
                      TextFieldClass('Enter your zipcode',
                          textEditingController: zipController,
                          enable: enableEdit,
                          keyboardType: TextInputType.phone),
                      Padding(
                        padding: EdgeInsets.only(top: height(20)),
                        child: SimpleText('Address',
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: greyDarkColor),
                      ),
                      TextFieldMaxLines('Enter your address',
                          textEditingController: addressController,
                          enable: enableEdit,
                          keyboardType: TextInputType.multiline,
                          maxLines: 5),
                      Padding(
                        padding: EdgeInsets.only(top: height(20)),
                        child: SimpleText('Speciality',
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: greyDarkColor),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      FutureBuilder(
                        future:
                            professionalServices.getSpecialities(addlist: true),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            specialitiesModel =
                                snapshot.data as List<SpecialtiesModel>;
                            List<String> languageNames = specialitiesModel
                                .map<String>(
                                    (languageModel) => languageModel.name)
                                .toList();
                            return CustomDropdownWidget(
                              defaultValue: "Select your speciality",
                              selectedValue: _selectSpeciality,
                              itemList: languageNames,
                              onValueChanged: (newValue) {
                                setState(() {
                                  _selectSpeciality = newValue!;
                                });
                              },
                              validator: (value) {
                                if (value == "Select your speciality") {
                                  return 'Please select speciality';
                                }
                                if (value == null) {
                                  return 'Please select speciality';
                                }
                              },
                            );
                          }

                          return Container();
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: height(20)),
                        child: SimpleText('Experience',
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: greyDarkColor),
                      ),
                      TextFieldClass('Enter your Experience',
                          textEditingController: experienceController,
                          enable: enableEdit,
                          keyboardType: TextInputType.phone),
                      Padding(
                        padding: EdgeInsets.only(top: height(20)),
                        child: SimpleText('Price',
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: greyDarkColor),
                      ),
                      TextFieldClass('Enter your price',
                          textEditingController: priceController,
                          enable: enableEdit,
                          keyboardType: TextInputType.phone),
                      SizedBox(height: height(20)),
                      Button(
                        'Save',
                        onTap: () {
                          professionalServices.updateProfessionalProfile(
                              name: nameController.text,
                              image: profileImage,
                              zip_code: zipController.text,
                              address: addressController.text,
                              experience: experienceController.text,
                              specialityId: specialitiesModel
                                  .where((element) =>
                                      element.name == _selectSpeciality)
                                  .first
                                  .id
                                  .toString(),
                              phone: phoneController.text,
                              context: context);
                          setState(() {
                            enableEdit = false;
                          });
                        },
                      ),
                      SizedBox(height: height(20)),
                    ],
                  ),
                ),
              ),
            )),
          ],
        ),
      ),
    );
  }

  toast({required String message}) {
    return Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.grey[700],
        textColor: Colors.white,
        fontSize: 14.0);
  }
}
