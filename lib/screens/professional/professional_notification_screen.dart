import 'package:castro_app/screens/core/professional_bottom_navigation_bar.dart';
import 'package:castro_app/services/update_fcm.dart';
import 'package:castro_app/widgets/willPopScope.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../models/notification_model.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/scalling.dart';
import '../../widgets/simple_text.dart';

class ProfessionalNotificationScreen extends StatefulWidget {
  const ProfessionalNotificationScreen({Key? key}) : super(key: key);

  @override
  _ProfessionalNotificationScreenState createState() =>
      _ProfessionalNotificationScreenState();
}

class _ProfessionalNotificationScreenState
    extends State<ProfessionalNotificationScreen> {
  FirebaseServices firebaseServices = FirebaseServices();
  List<NotificationModel> notificationList = [];

  @override
  Widget build(BuildContext context) {
    init(context);
    return WillPopScope(
      onWillPop: () async {
        return willPopScope(context, ProfessionalBottomNavBar());
      },
      child: Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          backgroundColor: whiteColor,
          // leading: InkWell(
          //   onTap: () {
          //     Navigator.pop(context);
          //   },
          //   child: Padding(
          //     padding: EdgeInsets.all(20),
          //     child: SvgPicture.asset(
          //       'assets/back_button.svg',
          //       color: blackColor3,
          //     ),
          //   ),
          // ),
          centerTitle: true,
          title: SimpleText('Notifications', color: blackColor3),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: height(10)),
            Expanded(
              child: FutureBuilder(
                future: firebaseServices.getNotification(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    notificationList = snapshot.data as List<NotificationModel>;
                    return notificationList.isEmpty
                        ? Center(
                            child: Text("No Notification found"),
                          )
                        : ListView.builder(
                            itemCount: notificationList.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                color: Colors.grey[200],
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12, vertical: 12),
                                  child: Row(
                                    children: [
                                      CircleAvatar(
                                        backgroundColor: Colors.black,
                                        radius: 6,
                                      ),
                                      SizedBox(
                                        width: 12,
                                      ),
                                      Expanded(
                                          child: Text(
                                        notificationList[index].body.toString(),
                                      )),
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                  }
                  return Center(
                    child: Text("No Notification found"),
                  );
                },
              ),
            ),
          ],
        ),
        // Column(
        //   crossAxisAlignment: CrossAxisAlignment.start,
        //   children: [
        //     SizedBox(height: height(10)),
        //     Expanded(
        //       child: ListView.separated(
        //         itemCount: 10,
        //         separatorBuilder: (BuildContext context, int index) =>
        //             SizedBox(height: height(30)),
        //         itemBuilder: (BuildContext context, int index) {
        //           return Column(
        //             children: [
        //               if (index == 0) SizedBox(height: height(10)),
        //               Padding(
        //                 padding: EdgeInsets.symmetric(horizontal: width(20)),
        //                 child: Row(
        //                   children: [
        //                     Padding(
        //                       padding: EdgeInsets.only(right: width(15)),
        //                       child: CircleAvatar(
        //                         radius: width(40),
        //                         backgroundImage:
        //                         const AssetImage('assets/list_bg.png'),
        //                       ),
        //                     ),
        //                     Expanded(
        //                       child: Column(
        //                         crossAxisAlignment: CrossAxisAlignment.start,
        //                         children: [
        //                           RichText(
        //                             text: TextSpan(
        //                               style: GoogleFonts.poppins(
        //                                   textStyle: TextStyle(
        //                                       color: blackColor,
        //                                       fontWeight: FontWeight.w600,
        //                                       fontSize: width(13))),
        //                               children: <TextSpan>[
        //                                 const TextSpan(text: 'Estheticians '),
        //                                 TextSpan(
        //                                   text:
        //                                   'has open a new consumer site. you’re invited to her new address',
        //                                   style: GoogleFonts.poppins(
        //                                       textStyle: TextStyle(
        //                                           fontWeight: FontWeight.normal,
        //                                           fontSize: width(13))),
        //                                 ),
        //                               ],
        //                             ),
        //                           ),
        //                           SimpleText(
        //                             '1 hour ago',
        //                             fontWeight: FontWeight.w500,
        //                             fontSize: 13,
        //                           ),
        //                         ],
        //                       ),
        //                     )
        //                   ],
        //                 ),
        //               ),
        //               if (index == 9) SizedBox(height: height(20)),
        //             ],
        //           );
        //         },
        //       ),
        //     ),
        //   ],
        // ),
      ),
    );
  }
}
