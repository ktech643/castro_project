import 'package:castro_app/models/professional/appointment_model.dart';
import 'package:castro_app/screens/core/professional_bottom_navigation_bar.dart';
import 'package:castro_app/services/professional_services.dart';
import 'package:castro_app/utilis/secure_storage.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/willPopScope.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../utilis/constants.dart';
import '../../widgets/button.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/simple_text.dart';

class AppointmentDetails extends StatefulWidget {
  AppointmentModel? appointmentModel;

  AppointmentDetails({this.appointmentModel});

  @override
  _AppointmentDetailsState createState() => _AppointmentDetailsState();
}

class _AppointmentDetailsState extends State<AppointmentDetails> {
  ProfessionalServices professionalServices = ProfessionalServices();
  SecureStorage secureStorage = SecureStorage();
  String? price;
  String? date;

  readPrice() {
    secureStorage.readStore("price").then((value) {
      setState(() {
        price = value;
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    readPrice();
    getDate();
  }

  getDate() {
    if (widget.appointmentModel != null) {
      var temp = widget.appointmentModel!.booking_date!.split(" ");
      setState(() {
        date = temp[0];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    init(context);
    return WillPopScope(
      onWillPop: () async {
        return willPopScope(context, ProfessionalBottomNavBar());
      },
      child: Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          elevation: 5,
          backgroundColor: whiteColor,
          centerTitle: true,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Padding(
              padding: EdgeInsets.all(20),
              child: SvgPicture.asset(
                'assets/back_button.svg',
              ),
            ),
          ),
          title: SimpleText('Appointment details',
              fontWeight: FontWeight.w500, fontSize: 16, color: blackColor3),
        ),
        body: widget.appointmentModel != null
            ? SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: height(36.68)),
                    Container(
                        height: height(91),
                        width: width(91),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(width(10)),
                          gradient: const LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [
                              orangeColor2,
                              pinkColor,
                              orangeColor,
                            ],
                          ),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(width(4)),
                          child: widget
                                      .appointmentModel!.customer_user!.image !=
                                  null
                              ? Container(
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.circular(width(10)),
                                      image: DecorationImage(
                                          image: NetworkImage(
                                              Constants.IMAGE_URL +
                                                  widget.appointmentModel!
                                                      .customer_user!.image
                                                      .toString()),
                                          fit: BoxFit.contain)),
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.circular(width(10)),
                                      image: DecorationImage(
                                          image:
                                              AssetImage("assets/profile.png"),
                                          fit: BoxFit.fill)),
                                ),
                        )),
                    SizedBox(height: height(13.62)),
                    SimpleText(widget.appointmentModel!.customer_user!.name,
                        fontWeight: FontWeight.w600),
                    SizedBox(height: height(2)),
                    SimpleText(widget.appointmentModel!.customer_user!.email,
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        color: greyDarkColor0),
                    SizedBox(height: height(31)),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.call, color: greyDarkColor3aa),
                                  SizedBox(width: width(12)),
                                  SimpleText(
                                      widget.appointmentModel!.customer_user!
                                          .phone,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 12,
                                      color: greyDarkColor0),
                                ]),
                          ),
                          Container(
                            height: height(20),
                            width: width(2),
                            color: greyDarkColor3aa,
                          ),
                          Expanded(
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset('assets/calendar.svg',
                                      color: greyDarkColor3aa),
                                  SizedBox(width: width(14)),
                                  SimpleText('0 yrs',
                                      fontWeight: FontWeight.w400,
                                      fontSize: 12,
                                      color: greyDarkColor0),
                                ]),
                          ),
                        ]),
                    SizedBox(height: height(35)),
                    itemsPrices("Fee", "\$ $price"),
                    ListView.builder(
                        physics: ScrollPhysics(),
                        itemCount: widget
                            .appointmentModel!.booked_sub_services!.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          var item = widget
                              .appointmentModel!.booked_sub_services![index];
                          return itemsPrices(
                              item.name.toString(), '\$ ${item.price}');
                        }),
                    itemsPrices(
                        "Total", "\$ ${widget.appointmentModel!.total_amount}"),
                    Container(
                      decoration: BoxDecoration(
                          color: greyLightColor3,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(width(25)),
                              topLeft: Radius.circular(width(25)))),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: width(20)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: height(30)),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SimpleText('Appointment Starting',
                                    fontWeight: FontWeight.w400, fontSize: 14),
                                // SimpleText('Cancel',
                                //     fontWeight: FontWeight.w500,
                                //     fontSize: 14,
                                //     color: redColor),
                              ],
                            ),
                            SizedBox(height: height(6)),
                            Row(
                              children: [
                                SimpleText('Date : ',
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12),
                                SizedBox(
                                  width: 20,
                                ),
                                SimpleText(date, color: pinkColor),
                              ],
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Row(
                              children: [
                                SimpleText('Time : ',
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12),
                                SizedBox(
                                  width: 20,
                                ),
                                SimpleText(
                                    (widget.appointmentModel!.booking_time
                                        .toString()),
                                    color: pinkColor),
                              ],
                            ),
                            // SizedBox(height: height(20)),
                            // SimpleText('Time',
                            //     fontWeight: FontWeight.w400, fontSize: 14),
                            // SizedBox(height: height(9)),
                            // SimpleText('Not found',
                            //     fontWeight: FontWeight.w400,
                            //     fontSize: 12,
                            //     color: greyDarkColor3aaa),
                            SizedBox(height: height(61)),
                            widget.appointmentModel!.status == "completed"
                                ? Button('Completed')
                                : Button('Complete Appointment', onTap: () {
                                    professionalServices.completeAppoinment(
                                        context: context,
                                        book_specialist_id:
                                            widget.appointmentModel!.id);
                                    // Navigator.of(context).push(MaterialPageRoute(
                                    //     builder: (context) =>
                                    //         AppointmentCompleted())
                                    // );
                                  }),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : Center(
                child: Text("No Detail found"),
              ),
      ),
    );
  }

  Column itemsPrices(itemsName, price) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Container(
            height: height(45),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SimpleText(itemsName,
                    fontWeight: FontWeight.w500,
                    fontSize: 12,
                    color: greyDarkColor),
                SimpleText(price,
                    fontWeight: FontWeight.w600,
                    fontSize: 14,
                    color: greyDarkColor),
              ],
            ),
          ),
        ),
        Divider(),
      ],
    );
  }
}
