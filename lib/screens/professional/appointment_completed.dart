import 'package:castro_app/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/scalling.dart';
import '../../widgets/simple_text.dart';

class AppointmentCompleted extends StatefulWidget {
  const AppointmentCompleted({Key? key}) : super(key: key);

  @override
  _AppointmentCompletedState createState() => _AppointmentCompletedState();
}

class _AppointmentCompletedState extends State<AppointmentCompleted> {
  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      backgroundColor: scaffoldBgColor,
      appBar: AppBar(
        backgroundColor: whiteColor,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.all(20),
            child: SvgPicture.asset(
              'assets/back_button.svg',
              color: blackColor3,
            ),
          ),
        ),
        centerTitle: true,
        title: SimpleText('Completed Appointments', color: blackColor3),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ListView.separated(
              itemCount: 10,
              separatorBuilder: (BuildContext context, int index) =>
                  SizedBox(height: height(16)),
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  children: [
                    if (index == 0) SizedBox(height: height(10)),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: width(21)),
                      child: Card(
                        color: whiteColor,
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(width(10)),
                        ),
                        child: Container(
                          height: height(122),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: width(16)),
                            child: Center(
                              child: Container(
                                height: height(92),
                                child: Row(
                                  children: [
                                    Container(
                                      height: height(89.06),
                                      width: width(85.76),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                          BorderRadius.circular(width(10)),
                                          color: whiteColor,
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'assets/booking_bg.png'), fit: BoxFit.fill)),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: EdgeInsets.only(left: width(10)),
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                SimpleText(
                                                  'Dr. John Doe',
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 14,
                                                  color: blackColor3,
                                                ),
                                                SimpleText(
                                                  '\$50.00',
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 14,
                                                  color: pinkColor,
                                                ),
                                              ],
                                            ),
                                            SimpleText(
                                              'Thursday 17, Feb',
                                              fontWeight: FontWeight.w500,
                                              fontSize: 10,
                                              color: pinkColor,
                                            ),
                                            SmallButton('Completed', isCompleted: true),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    if (index == 9) SizedBox(height: height(20)),
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
