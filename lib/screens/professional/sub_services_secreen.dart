import 'package:castro_app/screens/professional/create_services.dart';
import 'package:castro_app/screens/professional/update_subService.dart';
import 'package:castro_app/services/professional_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../models/professional/professional_user_model.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/scalling.dart';
import '../../widgets/simple_text.dart';

class SubServicesSecreen extends StatefulWidget {
  const SubServicesSecreen({Key? key}) : super(key: key);

  @override
  _SubServicesSecreenState createState() => _SubServicesSecreenState();
}

class _SubServicesSecreenState extends State<SubServicesSecreen> {
  TextEditingController title = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController price = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  List<SubServicesModel> subServicesList = [];
  ProfessionalServices professionalServices = ProfessionalServices();

  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        elevation: 5,
        backgroundColor: whiteColor,
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.all(20),
            child: SvgPicture.asset(
              'assets/back_button.svg',
            ),
          ),
        ),
        title: SimpleText('Sub Services',
            fontWeight: FontWeight.w600, fontSize: 18, color: blackColor),
      ),
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: width(20)),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FutureBuilder(
                  future: professionalServices.getSubServices(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      subServicesList = snapshot.data as List<SubServicesModel>;
                      print(subServicesList.length);
                      return subServicesList.isEmpty
                          ? Center(
                              child: Text("No service found"),
                            )
                          : ListView.separated(
                              itemCount: subServicesList.length,
                              shrinkWrap: true,
                              separatorBuilder:
                                  (BuildContext context, int index) =>
                                      SizedBox(height: height(16)),
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                  children: [
                                    if (index == 0)
                                      SizedBox(height: height(10)),
                                    Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 5),
                                      child: Card(
                                        color: whiteColor,
                                        elevation: 3,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(width(10)),
                                        ),
                                        child: Container(
                                          height: height(100),
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: width(18),
                                                vertical: width(10)),
                                            child: Column(
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    SimpleText(
                                                      subServicesList[index]
                                                          .name,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 14,
                                                      color: blackColor3,
                                                    ),
                                                    SimpleText(
                                                      subServicesList[index]
                                                                  .price !=
                                                              null
                                                          ? '\$${subServicesList[index].price}.00'
                                                          : "\$ 0.00",
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 14,
                                                      color: pinkColor,
                                                    ),
                                                  ],
                                                ),
                                                Spacer(),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Flexible(
                                                      child: Text(
                                                        "${subServicesList[index].description != null ? subServicesList[index].description : "Description not found."}",
                                                        softWrap: true,
                                                        maxLines: 3,
                                                        overflow: TextOverflow
                                                            .visible,
                                                        style: TextStyle(
                                                            fontSize: 12,
                                                            color: Colors
                                                                .grey[600]),
                                                      ),
                                                    ),
                                                    Container(
                                                      child: Row(
                                                        children: [
                                                          GestureDetector(
                                                            onTap: () {
                                                              Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                      builder: (context) =>
                                                                          UpdateServices(
                                                                            subServicesModel:
                                                                                subServicesList[index],
                                                                          ))).then(
                                                                  (_) => setState(
                                                                      () {}));
                                                            },
                                                            child: Icon(
                                                              Icons.edit,
                                                              color:
                                                                  Colors.blue,
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: 20,
                                                          ),
                                                          GestureDetector(
                                                            onTap: () {
                                                              professionalServices
                                                                  .deleteSubServices(
                                                                      id: subServicesList[
                                                                              index]
                                                                          .id
                                                                          .toString(),
                                                                      context:
                                                                          context)
                                                                  .then(
                                                                      (value) {
                                                                setState(() {});
                                                              });
                                                            },
                                                            child: Icon(
                                                              Icons.delete,
                                                              color: Colors.red,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    if (index == 9)
                                      SizedBox(height: height(20)),
                                  ],
                                );
                              },
                            );
                    }
                    return const Center(
                      child: Text("No service found"),
                    );
                  },
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.pink,
        onPressed: () {
          Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CreateServices()))
              .then((_) => setState(() {}));
        },
      ),
    );
  }
}
