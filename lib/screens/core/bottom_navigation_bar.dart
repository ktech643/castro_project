import 'package:castro_app/screens/core/tabBarScreens.dart';
import 'package:castro_app/screens/customer/profile_screen.dart';
import 'package:castro_app/widgets/primaryColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../widgets/scalling.dart';
import '../customer/booking_screen.dart';
import '../customer/notification_screen.dart';

class BottomNavBar extends StatefulWidget {
  const BottomNavBar({Key? key}) : super(key: key);

  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int _selectedIndex = 0;
  static const List<Widget> _widgetOptions = <Widget>[
    TabBarScreens(),
    NotificationScreen(),
    BookingScreen(),
    ProfileScreen()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/home.svg', color: blackColor3),
            activeIcon: SvgPicture.asset('assets/home.svg', color: pinkColor2),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/notifications.svg',
                color: blackColor3),
            activeIcon:
                SvgPicture.asset('assets/notifications.svg', color: pinkColor2),
            label: 'Notification',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/booking.svg', color: blackColor3),
            activeIcon:
                SvgPicture.asset('assets/booking.svg', color: pinkColor2),
            label: 'Booking',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/profile.svg', color: blackColor3),
            activeIcon:
                SvgPicture.asset('assets/profile.svg', color: pinkColor2),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: pinkColor2,
        unselectedItemColor: blackColor,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        selectedLabelStyle: GoogleFonts.poppins(
            textStyle: TextStyle(
                fontSize: width(8),
                color: blackColor,
                fontWeight: FontWeight.w600)),
        unselectedLabelStyle: GoogleFonts.poppins(
            textStyle: TextStyle(
                fontSize: width(8),
                color: blackColor,
                fontWeight: FontWeight.w600)),
        onTap: _onItemTapped,
      ),
    );
  }
}
