import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:castro_app/models/professional/professional_user_model.dart';
import 'package:castro_app/models/professional/specialities_list.dart';
import 'package:castro_app/screens/StartingScreens/signin_screen.dart';
import 'package:castro_app/services/professional_services.dart';
import 'package:castro_app/utilis/utilis.dart';
import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/simple_text.dart';
import 'package:castro_app/widgets/willPopScope.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../utilis/constants.dart';
import '../../utilis/secure_storage.dart';
import '../../widgets/specialities_custom_button.dart';
import 'dart:async';
import 'dart:ui' as ui;
import 'package:castro_app/widgets/specialist_container.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../customer/doctor_details.dart';

int? appTabBarIndex;

class TabBarScreens extends StatefulWidget {
  const TabBarScreens({Key? key}) : super(key: key);

  @override
  _TabBarScreensState createState() => _TabBarScreensState();
}

class _TabBarScreensState extends State<TabBarScreens> {
  final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key
  SecureStorage secureStorage = SecureStorage();
  ProfessionalServices professionalServices = ProfessionalServices();
  String? name;
  String? email;
  List<SpecialtiesModel>? specialitiesModel;
  List<ProfessionalUserModel> professionalUserList = List.empty(growable: true);
  Completer<GoogleMapController> _mapController = Completer();
  LocationPermission? permission;
  ProfessionalUserModel? professionalUserModel;
  GoogleMapController? _googleMapController;
  LatLng currentPostion = LatLng(37.42796133580664, -122.085749655962);
  List<Marker> _markers = [];
  String? _mapStyle;
  bool loading = false;
  TextStyle textStyle = TextStyle(
    fontFamily: 'Roboto',
    fontSize: 17,
    color: Colors.black,
  );
  bool professionalLoading = false;

  // static final List<Widget> _tabsList = <Widget>[
  //   Tab(text: "Estheticians"),
  //   Tab(text: "Natural Hair Stylist"),
  //   Tab(text: "Nails Specialist"),
  //   Tab(text: "Barber"),
  //   Tab(text: "Cosmetologist"),
  // ];
  //
  // static List<Widget> _tabBarActionList = <Widget>[
  //   HomeMap(),
  //   HomeMap(),
  //   HomeMap(),
  //   HomeMap(),
  //   HomeMap(),
  //   // HomeScreen(),
  //   // HomeScreen(),
  //   // HomeScreen(),
  //   // HomeScreen()
  // ];
  int? selectedId;
  bool isLoading = false;
  Validator validator = Validator();

  @override
  void initState() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      getNotification(message);
    });
    // TODO: implement initState
    super.initState();
    secureStorage.readStore("name").then((value) {
      setState(() {
        name = value;
      });
    });
    secureStorage.readStore("email").then((value) {
      setState(() {
        email = value;
      });
    });
    professionalServices.getSpecialities(addlist: false).then((value) {
      if (mounted) {
        setState(() {
          specialitiesModel = value;
          selectedId = specialitiesModel![0].id;
          _getUserLocation(selectedId);
          professionalServices
              .getProfessionalUserById(speciality_id: selectedId)
              .then((value) {
            setState(() {
              professionalUserList = value;
              professionalLoading = true;
            });
          });
          isLoading = true;
        });
      }
    });
  }

  Future<Uint8List> _getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  _showSpecialitiesOnMap(BuildContext context, int? specialityId) async {
    _markers.clear();
    // Position position = await Helper.getUserLatLng(context);
    professionalServices
        .getProfessionalUserById(speciality_id: specialityId)
        .then((value) {
      setState(() {
        professionalUserList = value;
      });
      _getBytesFromAsset("assets/map.png", 80).then((markerIcon) {
        professionalUserList.forEach((professional) {
          _markers.add(
            Marker(
              markerId: MarkerId(professional.id.toString()),
              position: LatLng(double.parse(professional.latitude.toString()),
                  double.parse(professional.longitude.toString())),
              icon: BitmapDescriptor.fromBytes(markerIcon),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => DoctorDetails(
                          professionalUserModel: professional,
                        )));
              },
            ),
          );
        });
        LatLng currentPosition =
            LatLng(currentPostion.latitude, currentPostion.longitude);
        LatLngBounds mBound = boundMap(_markers, currentPosition);
        CameraUpdate cameraUpdate = CameraUpdate.newLatLngBounds(mBound, 0);
        _googleMapController!
            .animateCamera(cameraUpdate)
            .then((value) => check(cameraUpdate, _googleMapController!));
        setState(() {});
      });
    });
  }

  void check(CameraUpdate u, GoogleMapController c) async {
    c.animateCamera(u);
    _googleMapController!.animateCamera(u);
    LatLngBounds l1 = await c.getVisibleRegion();
    LatLngBounds l2 = await c.getVisibleRegion();
    if (l1.southwest.latitude == -90 || l2.southwest.latitude == -90)
      check(u, c);
  }

  LatLngBounds boundMap(List<Marker> markers, LatLng initLatLng) {
    LatLngBounds latLngBounds =
        LatLngBounds(southwest: initLatLng, northeast: initLatLng);
    LatLng currentLatLng = initLatLng;
    for (final marker in markers) {
      if (currentLatLng.latitude > marker.position.latitude &&
          currentLatLng.longitude > marker.position.longitude) {
        latLngBounds =
            LatLngBounds(southwest: marker.position, northeast: currentLatLng);
      } else if (currentLatLng.longitude > marker.position.longitude) {
        latLngBounds = LatLngBounds(
            southwest:
                LatLng(currentLatLng.latitude, marker.position.longitude),
            northeast:
                LatLng(marker.position.latitude, currentLatLng.longitude));
      } else if (currentLatLng.latitude > marker.position.latitude) {
        latLngBounds = LatLngBounds(
            southwest:
                LatLng(marker.position.latitude, currentLatLng.longitude),
            northeast:
                LatLng(currentLatLng.latitude, marker.position.longitude));
      } else {
        currentLatLng = marker.position;
        latLngBounds =
            LatLngBounds(southwest: currentLatLng, northeast: marker.position);
      }
    }

    return latLngBounds;
  }

  @override
  Widget build(BuildContext context) {
    init(context);
    return WillPopScope(
      onWillPop: () async {
        return showExitAlertDialog(context);
      },
      child: Scaffold(
        key: _key,
        backgroundColor: whiteColor,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(height: height(52)),
              Padding(
                padding: EdgeInsets.only(right: width(20), left: width(10)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                        onTap: () {
                          _key.currentState!.openDrawer();
                        },
                        child: Padding(
                          padding: EdgeInsets.all(width(10)),
                          child: SvgPicture.asset('assets/drawer.svg'),
                        )),
                    Container(
                      height: height(54),
                      width: width(295),
                      child: TypeAheadField(
                        textFieldConfiguration: TextFieldConfiguration(
                          // inputFormatters: [
                          //   UpperCaseTextFormatter(),
                          // ],
                          textCapitalization: TextCapitalization.words,
                          style: textStyle,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: greyLightColor3,
                            focusColor: greyLightColor3,
                            border: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            labelText: 'Search for specialist',
                            labelStyle: TextStyle(
                              color: blackColor,
                            ),
                            hintText: 'Search for specialist',
                            hintStyle: TextStyle(
                              color: blackColor,
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                                  const BorderSide(color: greyLightColor3),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(color: pinkColor),
                            ),
                            suffixIcon: Container(
                              width: width(59),
                              decoration: BoxDecoration(
                                gradient: const LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [
                                    orangeColor2,
                                    pinkColor,
                                    orangeColor,
                                  ],
                                ),
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(width(10)),
                                    bottomRight: Radius.circular(width(12))),
                              ),
                              child:
                                  Icon(Icons.arrow_forward, color: whiteColor),
                            ),
                          ),
                        ),
                        suggestionsCallback: (pattern) async {
                          if (pattern == null || pattern.length < 3) {
                            return [];
                          }
                          List<ProfessionalUserModel> list = [];
                          final value = await professionalServices
                              .getProfessionalUserByName(name: pattern);
                          list = value;
                          return list;
                        },
                        hideOnEmpty: true,
                        noItemsFoundBuilder: (context) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("No User Found"),
                          );
                        },
                        itemBuilder: (context, professionalUserModel) {
                          ProfessionalUserModel prof = ProfessionalUserModel();
                          prof = professionalUserModel as ProfessionalUserModel;
                          return ListTile(
                            title: Text(prof.name.toString()),
                          );
                        },
                        onSuggestionSelected: (player) {
                          ProfessionalUserModel prof = ProfessionalUserModel();
                          prof = player as ProfessionalUserModel;
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DoctorDetails(
                                        professionalUserModel: prof,
                                      )));
                          // _searchPlayerByName(
                          //     context: context,
                          //     sizingInformation: sizingInformation,
                          //     name: player.fullName);
                          // setState(() {
                          //   name = player.fullName;
                          // });
                          // Navigator.of(context).push(MaterialPageRoute(
                          //     builder: (context) => ProductPage(product: suggestion)
                          // ));
                        },
                      ),
                      // TextFormField(
                      //   enableSuggestions: false,
                      //   autocorrect: false,
                      //   cursorColor: Colors.grey,
                      //   keyboardType: TextInputType.text,
                      //   style: GoogleFonts.roboto(
                      //       textStyle: const TextStyle(
                      //     color: blackColor,
                      //   )),
                      //   onFieldSubmitted: (value) {
                      //     FocusScope.of(context).unfocus();
                      //   },
                      //   decoration: InputDecoration(
                      //     filled: true,
                      //     fillColor: greyLightColor3,
                      //     focusColor: greyLightColor3,
                      //     border: InputBorder.none,
                      //     errorBorder: InputBorder.none,
                      //     disabledBorder: InputBorder.none,
                      //     labelText: 'Search for specialist',
                      //     labelStyle: TextStyle(
                      //       color: blackColor,
                      //     ),
                      //     hintText: 'Search for specialist',
                      //     hintStyle: TextStyle(
                      //       color: blackColor,
                      //     ),
                      //     enabledBorder: OutlineInputBorder(
                      //       borderRadius: BorderRadius.circular(10),
                      //       borderSide:
                      //           const BorderSide(color: greyLightColor3),
                      //     ),
                      //     focusedBorder: OutlineInputBorder(
                      //       borderRadius: BorderRadius.circular(10),
                      //       borderSide: const BorderSide(color: pinkColor),
                      //     ),
                      //     suffixIcon: Container(
                      //       width: width(59),
                      //       decoration: BoxDecoration(
                      //         gradient: const LinearGradient(
                      //           begin: Alignment.centerLeft,
                      //           end: Alignment.centerRight,
                      //           colors: [
                      //             orangeColor2,
                      //             pinkColor,
                      //             orangeColor,
                      //           ],
                      //         ),
                      //         borderRadius: BorderRadius.only(
                      //             topRight: Radius.circular(width(10)),
                      //             bottomRight: Radius.circular(width(12))),
                      //       ),
                      //       child: Icon(Icons.arrow_forward, color: whiteColor),
                      //     ),
                      //   ),
                      // ),
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: 30,
              ),
              isLoading
                  ? SizedBox(
                      height: 35,
                      child: ListView.builder(
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          scrollDirection: Axis.horizontal,
                          itemCount: specialitiesModel!.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5),
                              child: CustomSubCategoryButton(
                                title: specialitiesModel![index].name,
                                textColor:
                                    selectedId == specialitiesModel![index].id
                                        ? whiteColor
                                        : blackColor,
                                bgColor:
                                    selectedId == specialitiesModel![index].id
                                        ? pinkColor2
                                        : greyLightColor3,
                                onPressed: () {
                                  setState(() {
                                    // professionalLoading = false;
                                    selectedId = specialitiesModel![index].id;
                                    _getUserLocation(selectedId);
                                    professionalServices
                                        .getProfessionalUserById(
                                            speciality_id: selectedId)
                                        .then((value) {
                                      setState(() {
                                        professionalUserList = value;
                                        professionalLoading = true;
                                      });
                                    });

                                    // storage.setStore('selectedId', selectedId);
                                  });
                                },
                              ),
                            );
                          }),
                    )
                  : CircularProgressIndicator(
                      color: pinkColor2,
                    ),
              SizedBox(height: height(20.5)),
              SizedBox(
                height: 200,
                child: loading
                    ? GoogleMap(
                        mapType: MapType.normal,
                        initialCameraPosition:
                            CameraPosition(target: currentPostion, zoom: 5),
                        onMapCreated: (GoogleMapController controller) => {
                          setState(() {
                            _googleMapController = controller;
                            _mapController.complete(controller);
                            _googleMapController?.setMapStyle(_mapStyle);
                          })
                        },
                        markers: Set.of(_markers),
                        gestureRecognizers: Set()
                          ..add(Factory<PanGestureRecognizer>(
                              () => PanGestureRecognizer())),
                        rotateGesturesEnabled: false,
                        compassEnabled: false,
                        myLocationEnabled: true,
                        myLocationButtonEnabled: true,
                        zoomGesturesEnabled: false,
                        zoomControlsEnabled: true,
                      )
                    : Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text("Check location permission."),
                            SizedBox(
                              height: 25,
                            ),
                            GestureDetector(
                              onTap: () {
                                _getUserLocation(selectedId);
                              },
                              child: Text(
                                "Try again",
                                style: TextStyle(color: pinkColor),
                              ),
                            )
                          ],
                        ),
                      ),

                // child: GoogleMap(
                //   mapType: MapType.normal,
                //   initialCameraPosition: _kGooglePlex,
                //   onMapCreated: (GoogleMapController controller) {
                //     _mapController.complete(controller);
                //   },
                // ),
              ),
              professionalLoading
                  ? professionalUserList.isEmpty
                      ? Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 7, horizontal: 7),
                          child: Text("No user found"),
                        )
                      : Container()
                  : Container(),
              professionalLoading
                  ? ListView.builder(
                      itemCount: professionalUserList.length,
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 7, horizontal: 7),
                          child: SpecialistContainer(
                              image: professionalUserList[index].image != null
                                  ? CircleAvatar(
                                      radius: 25,
                                      backgroundImage: NetworkImage(
                                          Constants.IMAGE_URL +
                                              professionalUserList[index]
                                                  .image
                                                  .toString()))
                                  : CircleAvatar(
                                      radius: 25,
                                      backgroundImage:
                                          AssetImage("assets/profile.png")),
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => DoctorDetails(
                                          professionalUserModel:
                                              professionalUserList[index],
                                        )));
                              },
                              name: professionalUserList[index].name!,
                              address:
                                  professionalUserList[index].address != null
                                      ? professionalUserList[index].address!
                                      : "",
                              time: "0 min"),
                        );
                      })
                  : Container(),
              // ButtonsTabBar(
              //     backgroundColor: pinkColor2,
              //     unselectedBackgroundColor: greyLightColor3,
              //     unselectedLabelSt  yle: GoogleFonts.poppins(
              //         textStyle: TextStyle(
              //             color: blackColor,
              //             fontWeight: FontWeight.w500,
              //             fontSize: 11.9)),
              //     labelStyle: GoogleFonts.poppins(
              //         textStyle: TextStyle(
              //             color: whiteColor,
              //             fontWeight: FontWeight.w500,
              //             fontSize: 11.9)),
              //     contentPadding: EdgeInsets.symmetric(horizontal: width(15)),
              //     radius: 100,
              //     tabs: _tabsList),
              // GestureDetector(
              //   child: Text("View By"),
              // ),
              // Expanded(
              //   child: TabBarView(
              //       physics: ScrollPhysics(), children: _tabBarActionList),
              // ),
            ],
          ),
        ),
        drawer: SafeArea(
          child: Drawer(
            backgroundColor: whiteColor,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  bottomRight: Radius.circular(30)),
            ),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: width(20), vertical: height(20)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                          alignment: Alignment.centerRight,
                          child: InkWell(
                              onTap: () {
                                if (_key.currentState!.isDrawerOpen) {
                                  _key.currentState!.openEndDrawer();
                                }
                              },
                              child: SvgPicture.asset('assets/cancel.svg'))),
                      SimpleText(name,
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          color: blackColor3),
                      SimpleText(email,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: greyDarkColor),
                    ],
                  ),
                ),
                SizedBox(height: height(62)),
                // ListTile(
                //   leading: SvgPicture.asset('assets/payment.svg'),
                //   title: SimpleText('Payment',
                //       fontWeight: FontWeight.w400, color: blackColor3),
                //   onTap: () {
                //     Navigator.of(context).push(MaterialPageRoute(
                //         builder: (context) => PaymentScreen()));
                //   },
                // ),
                // ListTile(
                //   leading: SvgPicture.asset('assets/premium_sub.svg'),
                //   title: const Text('Get Premium'),
                //   onTap: () {
                //     Navigator.of(context).push(MaterialPageRoute(
                //         builder: (context) => CustomerSubscription()));
                //   },
                // ),
                Expanded(child: Container()),
                ListTile(
                  leading: SvgPicture.asset('assets/logout.svg'),
                  title: const Text('Logout'),
                  onTap: () {
                    secureStorage.deleteAll();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => SigninScreen()),
                        (Route<dynamic> route) => false);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void getNotification(RemoteMessage message) async {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => Dialog(
        child: Container(
          height: 200,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Text(
                message.data['title'],
                style: TextStyle(
                    color: Colors.black,
                    fontFamily: "Segoe UI",
                    fontWeight: FontWeight.w800,
                    fontSize: 16),
              ),
              SizedBox(
                height: 40,
              ),
              Text.rich(
                TextSpan(
                  children: [
                    TextSpan(
                      text: message.data['body'],
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(right: 40, bottom: 20),
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Ok",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: Colors.pink,
                          fontFamily: "Segoe UI"),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _getUserLocation(int? specialityId) async {
    bool serviceEnabled;
    permission = await Geolocator.requestPermission();
    var position = await GeolocatorPlatform.instance.getCurrentPosition();
    _showSpecialitiesOnMap(context, specialityId);
    if (mounted) {
      setState(() {
        currentPostion = LatLng(position.latitude, position.longitude);
        loading = true;
      });
    }

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => Dialog(
          child: Container(
            height: 250,
            child: Padding(
              padding: EdgeInsets.all(12.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.location_on,
                    color: Colors.pink,
                    size: 28,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Use your location',
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: "Segoe UI",
                        fontWeight: FontWeight.w800,
                        fontSize: 18),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text:
                              'Castro wants to get your location so you can find your nearby Specialist. For furthur info please check our ',
                          style: TextStyle(fontSize: 16),
                        ),
                        TextSpan(
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              validator.launchPrivacyUrl();
                            },
                          text: "Privacy policy",
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.pink,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 12, vertical: 12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "No thanks",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.pink,
                                fontFamily: "Segoe UI"),
                          ),
                        ),
                        Spacer(),
                        GestureDetector(
                          onTap: () async {
                            // professionalServices.getProfessionalUserBYLocation(
                            //     speciality_id: selectedId,
                            //     latitude: currentPostion.latitude.toString(),
                            //     longitude: currentPostion.longitude.toString());
                            Navigator.pop(context);
                            permission = await Geolocator.requestPermission();
                            var position = await GeolocatorPlatform.instance
                                .getCurrentPosition();
                            _showSpecialitiesOnMap(context, specialityId);
                            setState(() {
                              currentPostion =
                                  LatLng(position.latitude, position.longitude);
                              loading = true;
                            });
                          },
                          child: Text(
                            "Turn on",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.pink,
                                fontFamily: "Segoe UI"),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    } else {
      var position = await GeolocatorPlatform.instance.getCurrentPosition();
      _showSpecialitiesOnMap(context, specialityId);
      setState(() {
        currentPostion = LatLng(position.latitude, position.longitude);

        // professionalServices.getProfessionalUserBYLocation(
        //     speciality_id: selectedId,
        //     latitude: currentPostion.latitude.toString(),
        //     longitude: currentPostion.longitude.toString());
        loading = true;
      });
    }
  }
}
