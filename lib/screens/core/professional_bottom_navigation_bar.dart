import 'package:castro_app/models/professional/appointment_model.dart';
import 'package:castro_app/screens/professional/appointment_details.dart';
import 'package:castro_app/screens/professional/professional_notification_screen.dart';
import 'package:castro_app/screens/professional/professional_profile_screen.dart';
import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import '../professional/professional_home_screen.dart';

class ProfessionalBottomNavBar extends StatefulWidget {
  int selectedIndex;
  AppointmentModel? appointmentModel;

  ProfessionalBottomNavBar(
      {Key? key, this.selectedIndex = 0, this.appointmentModel})
      : super(key: key);

  @override
  _ProfessionalBottomNavBarState createState() =>
      _ProfessionalBottomNavBarState();
}

class _ProfessionalBottomNavBarState extends State<ProfessionalBottomNavBar> {
  // int _selectedIndex = 0;
  List<Widget>? _widgetOptions;

  void _onItemTapped(int index) {
    setState(() {
      widget.selectedIndex = index;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _widgetOptions = <Widget>[
      ProfessionalHomeScreen(),
      ProfessionalNotificationScreen(),
      AppointmentDetails(
        appointmentModel: widget.appointmentModel,
      ),
      ProfessionalProfileScreen()
    ];
  }

  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      body: Container(
        child: Center(
          child: _widgetOptions!.elementAt(widget.selectedIndex),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/home.svg', color: blackColor3),
            activeIcon: SvgPicture.asset('assets/home.svg', color: pinkColor2),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/notifications.svg',
                color: blackColor3),
            activeIcon:
                SvgPicture.asset('assets/notifications.svg', color: pinkColor2),
            label: 'Notification',
          ),
          BottomNavigationBarItem(
            icon:
                SvgPicture.asset('assets/appointments.svg', color: blackColor3),
            activeIcon:
                SvgPicture.asset('assets/appointments.svg', color: pinkColor2),
            label: 'Appointments',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/profile.svg', color: blackColor3),
            activeIcon:
                SvgPicture.asset('assets/profile.svg', color: pinkColor2),
            label: 'Profile',
          ),
        ],
        currentIndex: widget.selectedIndex,
        selectedItemColor: pinkColor2,
        unselectedItemColor: blackColor,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        selectedLabelStyle: GoogleFonts.poppins(
            textStyle: TextStyle(
                fontSize: width(8),
                color: blackColor,
                fontWeight: FontWeight.w600)),
        unselectedLabelStyle: GoogleFonts.poppins(
            textStyle: TextStyle(
                fontSize: width(8),
                color: blackColor,
                fontWeight: FontWeight.w600)),
        onTap: _onItemTapped,
      ),
    );
  }
}
