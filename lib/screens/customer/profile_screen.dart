import 'dart:io';

import 'package:bottom_picker/bottom_picker.dart';
import 'package:bottom_picker/resources/arrays.dart';
import 'package:castro_app/models/professional/professional_user_model.dart';
import 'package:castro_app/screens/core/bottom_navigation_bar.dart';
import 'package:castro_app/services/customer_services.dart';
import 'package:castro_app/utilis/constants.dart';
import 'package:castro_app/utilis/secure_storage.dart';
import 'package:castro_app/widgets/button.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/textfield_class.dart';
import 'package:castro_app/widgets/willPopScope.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/simple_text.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  XFile? photo;
  final ImagePicker _picker = ImagePicker();
  String? profileImage;
  SecureStorage secureStorage = SecureStorage();
  CustomerServices customerServices = CustomerServices();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  ProfessionalUserModel? professionalUserModel;
  dynamic finalDate;
  bool imageLoading = false;
  bool enableEdit = false;
  bool isSubmit = false;
  bool isSmall = false;
  String text = 'Select your date of birth';

  Future<void> getImageCameraAndImage(bool isCamera) async {
    Navigator.of(context).pop();
    photo = await _picker.pickImage(
        source: isCamera == true ? ImageSource.camera : ImageSource.gallery);
    print('image: ${photo!.path}');
    if (photo != null) {
      setState(() {
        profileImage = photo!.path;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    customerServices.getUserProfile().then((value) {
      if (value != null) {
        setState(() {
          professionalUserModel = value;
          imageLoading = true;
          print(professionalUserModel!.image);
          _assignValue();
        });
      }
    });
  }

  _assignValue() {
    if (professionalUserModel!.name != null) {
      nameController.text = professionalUserModel!.name!;
    }
    if (professionalUserModel!.email != null) {
      emailController.text = professionalUserModel!.email!;
    }
    if (professionalUserModel!.phone != null) {
      phoneController.text = professionalUserModel!.phone!;
    }
    if (professionalUserModel!.dob != null) {
      text = professionalUserModel!.dob!;
    }
  }

  void showCameraGalleryAlert(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text(
              'Choose option',
              style:
                  TextStyle(fontWeight: FontWeight.w600, color: Colors.black),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  InkWell(
                    onTap: () {
                      getImageCameraAndImage(true);
                    },
                    splashColor: Colors.purpleAccent,
                    child: Row(
                      children: const [
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.camera,
                            color: pinkColor,
                          ),
                        ),
                        Text(
                          'Camera',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: pinkColor),
                        )
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      getImageCameraAndImage(false);
                    },
                    splashColor: Colors.purpleAccent,
                    child: Row(
                      children: const [
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.image,
                            color: pinkColor,
                          ),
                        ),
                        Text(
                          'Gallery',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: pinkColor),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    init(context);
    return WillPopScope(
      onWillPop: () async {
        return willPopScope(context, BottomNavBar());
      },
      child: Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: whiteColor,
          centerTitle: true,
          // leading: InkWell(
          //   onTap: () {
          //     Navigator.pop(context);
          //   },
          //   child: Padding(
          //     padding: EdgeInsets.all(20),
          //     child: SvgPicture.asset(
          //       'assets/back_button.svg',
          //     ),
          //   ),
          // ),
          title: SimpleText('My Profile',
              fontWeight: FontWeight.w600, fontSize: 18, color: blackColor),
        ),
        body: Column(
          children: [
            SizedBox(height: height(40)),
            Container(
              height: height(93),
              width: width(93),
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [
                    orangeColor2,
                    pinkColor,
                    orangeColor,
                  ],
                ),
              ),
              child: Stack(
                children: [
                  imageLoading
                      ? Center(
                          child: Container(
                            height: height(85),
                            width: width(85),
                            decoration: profileImage == null
                                ? professionalUserModel!.image == null
                                    ? BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: AssetImage(
                                              'assets/profile.png',
                                            )))
                                    : BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: NetworkImage(
                                              Constants.IMAGE_URL +
                                                  professionalUserModel!.image
                                                      .toString(),
                                            )))
                                : BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: FileImage(File(profileImage!)))),
                          ),
                        )
                      : SizedBox(),
                  InkWell(
                    onTap: () {
                      if (enableEdit == false) {
                        toast(message: "Enable editing option first.");
                      } else {
                        showCameraGalleryAlert(context);
                      }
                    },
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        height: height(30),
                        width: width(30),
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.3),
                              spreadRadius: 3,
                              blurRadius: 4,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ],
                          shape: BoxShape.circle,
                          color: whiteColor,
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(width(6)),
                          child: SvgPicture.asset('assets/edit_icon2.svg'),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: height(20)),
            SizedBox(height: height(42)),
            Expanded(
                child: Container(
              decoration: BoxDecoration(
                  color: greyLightColor3,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(width(25)),
                      topLeft: Radius.circular(width(25)))),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: width(20)),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: height(27)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(),
                          SimpleText('Details'),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                enableEdit = true;
                                toast(message: "Editing enabled");
                              });
                            },
                            child: SvgPicture.asset(
                              'assets/edit_icon.svg',
                              fit: BoxFit.fill,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: height(10.4)),
                      SimpleText('Name',
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: greyDarkColor),
                      TextFieldClass('Enter your name',
                          enable: enableEdit,
                          textEditingController: nameController,
                          keyboardType: TextInputType.emailAddress),
                      SizedBox(
                        height: 20,
                      ),
                      SimpleText('Email',
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: greyDarkColor),
                      TextFieldClass('Enter you email',
                          enable: false,
                          fillColor: Colors.grey.withOpacity(0.2),
                          textEditingController: emailController,
                          keyboardType: TextInputType.emailAddress),
                      SizedBox(
                        height: 20,
                      ),
                      SimpleText('Phone Number',
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: greyDarkColor),
                      TextFieldClass('Enter your phoneNo',
                          enable: enableEdit,
                          textEditingController: phoneController,
                          keyboardType: TextInputType.phone),
                      SizedBox(
                        height: 20,
                      ),
                      SimpleText('Date of Birth',
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: greyDarkColor),
                      _datePicker(),
                      SizedBox(height: height(20)),
                      Button(
                        'Save',
                        onTap: () {
                          customerServices.updateCustomerProfile(
                              name: nameController.text,
                              dob: finalDate != null ? finalDate : null,
                              image: profileImage,
                              phone: phoneController.text,
                              context: context);
                          setState(() {
                            enableEdit = false;
                          });
                        },
                      ),
                      SizedBox(height: height(20)),
                    ],
                  ),
                ),
              ),
            )),
          ],
        ),
      ),
    );
  }

  _datePicker() {
    return InkWell(
      onTap: () {
        // openDatePicker(context);
        if (enableEdit == false) {
        } else {
          BottomPicker.date(
            title: 'Select Date',
            titleStyle: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: width(15),
              color: orangeColor,
            ),
            dateOrder: DatePickerDateOrder.dmy,
            pickerTextStyle: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: width(13),
              color: blackColor,
            ),
            onChange: (index) {
              // print(index);
            },
            onSubmit: (index) {
              final dateFormat = DateFormat('yyyy-MM-dd');
              // print('finalDate...${dateFormat.format(index)}');
              setState(() {
                isSubmit = true;
                // finalDate = dateFormat.format(index);
                finalDate = dateFormat.format(index);
                text = finalDate;
              });
              print('submit...$finalDate');
            },
            bottomPickerTheme: BOTTOM_PICKER_THEME.orange,
          ).show(context);
        }
      },
      child: Container(
        height: isSmall ? height(54) : height(49),
        width: double.infinity,
        decoration: BoxDecoration(
            color: isSmall ? greyLightColor3 : whiteColor,
            borderRadius: BorderRadius.circular(width(10)),
            border: Border.all(color: borderColor)),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: width(18)),
          child: Row(
            mainAxisAlignment: isSmall
                ? MainAxisAlignment.spaceEvenly
                : MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(left: width(10)),
                child: SimpleText(text,
                    color: isSmall ? blackColor : greyDarkColor,
                    fontSize: isSmall ? 14 : 16),
              ),
              isSmall
                  ? isSubmit
                      ? SizedBox(height: 5, width: 5)
                      : Icon(Icons.arrow_drop_down)
                  : SvgPicture.asset('assets/calendar2.svg'),
            ],
          ),
        ),
      ),
    );
  }

  toast({required String message}) {
    return Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.grey[700],
        textColor: Colors.white,
        fontSize: 14.0);
  }
}
