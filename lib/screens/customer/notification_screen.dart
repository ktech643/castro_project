import 'package:castro_app/models/notification_model.dart';
import 'package:castro_app/screens/core/bottom_navigation_bar.dart';
import 'package:castro_app/services/update_fcm.dart';
import 'package:castro_app/widgets/willPopScope.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../widgets/primaryColor.dart';
import '../../widgets/scalling.dart';
import '../../widgets/simple_text.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  FirebaseServices firebaseServices = FirebaseServices();
  List<NotificationModel> notificationList = [];

  @override
  Widget build(BuildContext context) {
    init(context);
    return WillPopScope(
      onWillPop: () async {
        return willPopScope(context, BottomNavBar());
      },
      child: Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          backgroundColor: whiteColor,
          // leading: InkWell(
          //   onTap: () {
          //     Navigator.pop(context);
          //   },
          //   child: Padding(
          //     padding: EdgeInsets.all(20),
          //     child: SvgPicture.asset(
          //       'assets/back_button.svg',
          //       color: blackColor3,
          //     ),
          //   ),
          // ),
          centerTitle: true,
          title: SimpleText('Notifications', color: blackColor3),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: height(10)),
            Expanded(
              child: FutureBuilder(
                future: firebaseServices.getNotification(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    notificationList = snapshot.data as List<NotificationModel>;
                    return notificationList.isEmpty
                        ? Center(
                            child: Text("No Notification found"),
                          )
                        : ListView.builder(
                            itemCount: notificationList.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                color: Colors.grey[200],
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12, vertical: 12),
                                  child: Row(
                                    children: [
                                      CircleAvatar(
                                        backgroundColor: Colors.black,
                                        radius: 6,
                                      ),
                                      SizedBox(
                                        width: 12,
                                      ),
                                      Expanded(
                                          child: Text(
                                        notificationList[index].body.toString(),
                                      )),
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                  }
                  return Center(
                    child: Text("No Notification found"),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
