import 'package:castro_app/utilis/secure_storage.dart';
import 'package:castro_app/widgets/simple_text.dart';
import 'package:flutter/material.dart';

import '../../widgets/primaryColor.dart';
import '../../widgets/scalling.dart';
import 'doctor_details.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  SecureStorage secureStorage = SecureStorage();
  String? name;
  String? email;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    secureStorage.readStore("name").then((value) {
      if (value != null) {
        setState(() {
          name = value;
          print(name);
        });
      }
    });
    secureStorage.readStore("email").then((value) {
      if (value != null) {
        setState(() {
          email = value;
          print(email);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      backgroundColor: whiteColor,
      body: Column(
        children: [
          SizedBox(height: height(10)),
          Expanded(
            child: MediaQuery.removePadding(
              context: context,
              removeTop: true,
              child: ListView.separated(
                itemCount: 3,
                separatorBuilder: (BuildContext context, int index) =>
                    SizedBox(height: height(20)),
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: width(20)),
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>  DoctorDetails()));
                      },
                      child: Column(
                        children: [
                          Card(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(width(15))),
                            child: Container(
                              height: height(158),
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(width(10)),
                                  image: const DecorationImage(
                                      image:
                                          AssetImage('assets/home_list_bg.png'),
                                      fit: BoxFit.fill)),
                            ),
                          ),
                          SizedBox(height: height(10)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SimpleText(
                                    email,
                                    fontSize: 14,
                                    color: blackColor3,
                                  ),
                                  SizedBox(height: height(2)),
                                  SimpleText(
                                    'Esthetica Specialist',
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                    color: greyDarkColor2,
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                    height: height(40),
                                    width: width(138),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(width(30)),
                                        border: Border.all(color: pinkColor)),
                                    child: Center(
                                        child: SimpleText(
                                      'Book Appointment',
                                      fontSize: 12,
                                      color: pinkColor,
                                    )),
                                  )
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
