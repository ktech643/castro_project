import 'package:castro_app/services/customer_services.dart';
import 'package:castro_app/utilis/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../models/professional/professional_user_model.dart';
import '../../widgets/button.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/scalling.dart';
import '../../widgets/simple_text.dart';

class BookingDetailsScreen extends StatefulWidget {
  final List<SubServicesModel> cart;
  final String? bookingTime;
  final String? bookingDate;
  final ProfessionalUserModel? professionalUserModel;

  BookingDetailsScreen(this.cart, this.professionalUserModel, this.bookingTime,
      this.bookingDate);

  @override
  _BookingDetailsScreenState createState() => _BookingDetailsScreenState();
}

class _BookingDetailsScreenState extends State<BookingDetailsScreen> {
  double sum = 0;
  double totallPrice = 0;
  CustomerServices customerServices = CustomerServices();

  total() {
    widget.cart.forEach((element) {
      setState(() {
        sum = sum + int.parse(element.price.toString());
      });
    });
  }

  totalPrice() {
    setState(() {
      totallPrice =
          sum + double.parse(widget.professionalUserModel!.price.toString());
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    total();
    print("This is Time ${widget.bookingTime}");
    print("This is Date ${widget.bookingDate}");
    totalPrice();
  }

  @override
  Widget build(BuildContext context) {
    init(context);
    return WillPopScope(
      onWillPop: () async {
        return Navigator.canPop(context);
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: whiteColor,
        appBar: AppBar(
          backgroundColor: whiteColor,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Padding(
              padding: EdgeInsets.all(20),
              child: SvgPicture.asset(
                'assets/back_button.svg',
                color: blackColor3,
              ),
            ),
          ),
          centerTitle: true,
          title: SimpleText('Booking Details', color: blackColor3),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: width(20)),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: height(33)),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(right: width(15)),
                      child: widget.professionalUserModel!.image != null
                          ? CircleAvatar(
                              radius: width(40),
                              backgroundImage: NetworkImage(
                                  Constants.IMAGE_URL +
                                      widget.professionalUserModel!.image
                                          .toString()))
                          : CircleAvatar(
                              radius: width(40),
                              backgroundImage: AssetImage('assets/list_bg.png'),
                            ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SimpleText(
                            "${widget.professionalUserModel!.name?.toUpperCase()}",
                            fontWeight: FontWeight.w600,
                            fontSize: 15,
                          ),
                          // SimpleText(
                          //   'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
                          //   fontWeight: FontWeight.w500,
                          //   fontSize: 9,
                          // ),
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(height: height(35)),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SimpleText(
                        'Fee:  \$ ${widget.professionalUserModel!.price}',
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: blackColor),
                    SizedBox(height: height(15)),
                    ListView.builder(
                        itemCount: widget.cart.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          var item = widget.cart[index];
                          return itemsPrices(
                              item.name.toString(), '\$ ${item.price}');
                        }),
                    // Padding(
                    //   padding: EdgeInsets.only(right: width(10)),
                    //   child: Column(
                    //     crossAxisAlignment: CrossAxisAlignment.start,
                    //     children: [
                    //       itemsPrices('Amet Minim', '\$240'),
                    //       itemsPrices('Estheticians', '\$ 140'),
                    //       itemsPrices('Nail Technicians', '\$ 140'),
                    //       itemsPrices('Estheticians', '\$ 50'),
                    //     ],
                    //   ),
                    // ),
                    SizedBox(height: height(2)),
                    Container(
                      height: height(33),
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SimpleText('Total:',
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: blackColor),
                          SimpleText('\$ $totallPrice',
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: blackColor),
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(height: height(30)),
                Button('Booking Confirmation', onTap: () {
                  FocusScope.of(context).unfocus();
                  customerServices.bookProfessional(
                      context: context,
                      professionalId:
                          widget.professionalUserModel!.id.toString(),
                      bookingDate: widget.bookingDate,
                      price: totallPrice.toString(),
                      // ignore: unrelated_type_equality_checks
                      subServices: widget.cart,
                      bookingTime: widget.bookingTime != null
                          ? widget.bookingTime
                          : null);
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => const PaymentScreen()));
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Column itemsPrices(itemsName, price) {
    return Column(
      children: [
        Container(
          height: height(45),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SimpleText(itemsName,
                  fontWeight: FontWeight.w500,
                  fontSize: 12,
                  color: greyDarkColor),
              SimpleText(price,
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                  color: greyDarkColor),
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}
