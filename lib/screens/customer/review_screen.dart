import 'package:castro_app/models/professional/professional_user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';

import '../../widgets/primaryColor.dart';

class ReviewScreen extends StatefulWidget {
  final List<ReviewsModel>? reviewModel;

  const ReviewScreen({this.reviewModel});

  @override
  _ReviewScreenState createState() => _ReviewScreenState();
}

class _ReviewScreenState extends State<ReviewScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: whiteColor,
        title: Text(
          "Reviews",
          style: TextStyle(color: pinkColor),
        ),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.all(20),
            child: SvgPicture.asset(
              'assets/back_button.svg',
            ),
          ),
        ),
      ),
      body: SafeArea(
        child: widget.reviewModel!.isNotEmpty
            ? ListView.builder(
                padding: const EdgeInsets.all(4.0),
                physics: ScrollPhysics(),
                itemCount: widget.reviewModel!.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  var item = widget.reviewModel![index];
                  return Card(
                      elevation: 4.0,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 12, vertical: 12),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              item.review != null ? item.review.toString() : "",
                              textAlign: TextAlign.start,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            RatingBar.builder(
                              initialRating:
                                  double.parse(item.rating.toString()),
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: false,
                              itemCount: 5,
                              ignoreGestures: true,
                              itemSize: 20,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 0.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                            ),
                            Text(
                              "${item.user_name}",
                              style: TextStyle(
                                fontSize: 16,
                                color: pinkColor,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ));
                },
              )
            : Center(
                child: Text("No review found"),
              ),
      ),
    );
  }
}
