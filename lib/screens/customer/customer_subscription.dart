import 'package:castro_app/widgets/button.dart';
import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/simple_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomerSubscription extends StatefulWidget {
  const CustomerSubscription({Key? key}) : super(key: key);

  @override
  _CustomerSubscriptionState createState() => _CustomerSubscriptionState();
}

class _CustomerSubscriptionState extends State<CustomerSubscription> {
  @override
  Widget build(BuildContext context) {
    init(context);
    return Scaffold(
      backgroundColor: whiteColor,
        body: Stack(
          children: [
            Positioned(
                top: 0,
                right: 0,
                left: 0,
                bottom: 0,
                child: Image.asset('assets/premium_bg.png', fit: BoxFit.fill)),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: height(46)),
                InkWell(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: EdgeInsets.all(25),
                    child: SvgPicture.asset(
                      'assets/back_button.svg',
                    ),
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: width(20)),
                      child: Column(
                        children: [
                          SizedBox(height: height(4.5)),
                          // SvgPicture.asset('assets/premium.svg'),
                          Image.asset('assets/premium.png'),
                          SizedBox(height: height(26.43)),
                          SimpleText('Castro Premium', fontWeight: FontWeight.w600, fontSize: 24, color: blackColor3),
                          SizedBox(height: height(10)),
                          SimpleText('Unlimited access to premium \ncontent & features',
                              fontWeight: FontWeight.w400, fontSize: 12, color: blackColor4),
                          subscriptionAdvantages('Unlimited Storage', sizedBoxHeight: 25),
                          subscriptionAdvantages('Unlimited List, Board'),
                          subscriptionAdvantages('Unlimited Integratioon'),
                          subscriptionAdvantages('Unlimited Care with our expert'),
                          subscriptionAdvantages('Guest and Permissions & more'),
                          SizedBox(height: height(16)),
                          Button2('10.99', 'month'),
                          SizedBox(height: height(14)),
                          Button2('131.99', 'year'),
                          SizedBox(height: height(10)),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: SimpleText('You can save 30% for pay yearly',
                                fontWeight: FontWeight.w600, fontSize: 12, color: greyDarkColor),
                          ),
                          SizedBox(height: height(20)),
                          SimpleText('Continue Free With Ads',
                              fontWeight: FontWeight.w600, fontSize: 16, color: blackColor3),
                          SizedBox(height: height(20)),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ));
  }

  Column subscriptionAdvantages(String text, {double sizedBoxHeight = 8}) {
    return Column(
                children: [
                  SizedBox(height: height(sizedBoxHeight)),
                  Container(
                    height: height(50),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: greyLightColor3,
                      borderRadius: BorderRadius.circular(width(10))
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(left: width(24)),
                      child: Row(
                        children: [
                          SvgPicture.asset('assets/tick.svg', color: pinkLightColor),
                          SizedBox(width: width(5)),
                          SimpleText(text,
                              fontWeight: FontWeight.w400, fontSize: 14, color: blackColor4),
                        ],
                      ),
                    ),
                  ),
                ],
              );
  }
}
