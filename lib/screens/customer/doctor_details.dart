import 'package:bottom_picker/bottom_picker.dart';
import 'package:bottom_picker/resources/arrays.dart';
import 'package:castro_app/models/professional/professional_user_model.dart';
import 'package:castro_app/models/professional/specialities_list.dart';
import 'package:castro_app/screens/customer/review_screen.dart';
import 'package:castro_app/services/customer_services.dart';
import 'package:castro_app/widgets/button.dart';
import 'package:castro_app/widgets/primaryColor.dart';
import 'package:castro_app/widgets/scalling.dart';
import 'package:castro_app/widgets/simple_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../services/professional_services.dart';
import '../../utilis/constants.dart';
import 'booking_details_screen.dart';

class DoctorDetails extends StatefulWidget {
  final ProfessionalUserModel? professionalUserModel;

  DoctorDetails({this.professionalUserModel});

  @override
  _DoctorDetailsState createState() => _DoctorDetailsState();
}

class _DoctorDetailsState extends State<DoctorDetails> {
  DateTime _selectedValue = DateTime.now();
  String? finalTime = 'Select Time';
  bool isSubmit = false;
  CustomerServices customerServices = CustomerServices();
  ProfessionalServices professionalServices = ProfessionalServices();
  List<String> litems = ["1", "2", "Third", "4"];
  List<SubServicesModel> _cartList = <SubServicesModel>[];
  List<SpecialtiesModel> specialityList = List.empty(growable: true);
  double review = 0.0;

  // String specialityName = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    double temp =
        double.parse(widget.professionalUserModel!.reviews_avg.toString());
    double num1 = double.parse((temp).toStringAsFixed(2));
    setState(() {
      review = num1;
    });
  }

  // getSpecialityName() {
  //   professionalServices.getSpecialities(addlist: false).then((value) {
  //     specialityList = value;
  //     if (widget.professionalUserModel!.speciality_id != null) {
  //       setState(() {
  //         specialityName = specialityList
  //             .where((element) =>
  //                 element.id == widget.professionalUserModel!.speciality_id)
  //             .first
  //             .name;
  //       });
  //     }
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    init(context);
    return WillPopScope(
      onWillPop: () async {
        return Navigator.canPop(context);
      },
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: height(315),
                child: widget.professionalUserModel!.image != null
                    ? Image.network(
                        Constants.IMAGE_URL +
                            widget.professionalUserModel!.image.toString(),
                        fit: BoxFit.contain,
                      )
                    : Image.asset(
                        'assets/profile.png',
                        fit: BoxFit.contain,
                      ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //  SizedBox(height: height(20)),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Padding(
                      padding: EdgeInsets.all(width(20)),
                      child: SvgPicture.asset(
                        'assets/back_button.svg',
                        //color: whiteColor,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ],
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: height(550),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        topLeft: Radius.circular(30)),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: width(20)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: height(26)),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: SimpleText(
                                      widget.professionalUserModel!.name,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                      color: blackColor3,
                                    ),
                                  ),
                                  SimpleText(
                                    '\$ ${widget.professionalUserModel!.price != null ? widget.professionalUserModel!.price : "0"}',
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20,
                                    color: pinkColor,
                                  ),
                                ],
                              ),
                              // SizedBox(height: height(16)),
                              // SimpleText(
                              //   specialityName,
                              //   fontWeight: FontWeight.w500,
                              //   fontSize: 14,
                              //   color: greyDarkColor3a,
                              // ),
                              SizedBox(height: height(6)),
                              SimpleText(
                                widget.professionalUserModel!.address ?? "",
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: greyIconColor,
                              ),
                              SizedBox(height: height(16)),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  doctorDetails('Patients',
                                      'assets/patients.svg', '0', purpleColor),
                                  doctorDetails(
                                      'Experience',
                                      'assets/experience.svg',
                                      '${widget.professionalUserModel!.experience} yrs',
                                      greenColor),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ReviewScreen(
                                                    reviewModel: widget
                                                        .professionalUserModel!
                                                        .reviewModel,
                                                  )));
                                    },
                                    child: doctorDetails(
                                        'Reviews',
                                        'assets/review.svg',
                                        '${review}',
                                        yellowColor),
                                  ),
                                ],
                              ),
                              SizedBox(height: height(24)),
                            ],
                          ),
                        ),
                        Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  if (widget.professionalUserModel!.subservices!
                                      .isNotEmpty)
                                    Text(
                                      "Select Service",
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  // GestureDetector(
                                  //   child: Stack(
                                  //       alignment: Alignment.topCenter,
                                  //       children: <Widget>[
                                  //         Icon(
                                  //           Icons.panorama_photosphere_select,
                                  //           size: 30.0,
                                  //         ),
                                  //         if (_cartList.length > 0)
                                  //           Padding(
                                  //             padding: const EdgeInsets.only(
                                  //                 left: 2.0),
                                  //             child: CircleAvatar(
                                  //               radius: 8.0,
                                  //               backgroundColor: Colors.red,
                                  //               foregroundColor: Colors.white,
                                  //               child: Text(
                                  //                 _cartList.length.toString(),
                                  //                 style: TextStyle(
                                  //                   fontWeight: FontWeight.bold,
                                  //                   fontSize: 12.0,
                                  //                 ),
                                  //               ),
                                  //             ),
                                  //           ),
                                  //       ]),
                                  //   onTap: () {
                                  //     if (_cartList.isNotEmpty)
                                  //       Navigator.of(context).push(
                                  //         MaterialPageRoute(
                                  //           builder: (context) =>
                                  //               Cart(_cartList),
                                  //         ),
                                  //       );
                                  //   },
                                  // ),
                                ],
                              ),
                            ),
                            _buildGridView(),
                          ],
                        ),
                        Container(
                          // height: height(312),
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: greyLightColor3,
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30),
                                topLeft: Radius.circular(30)),
                          ),
                          child: Padding(
                            padding:
                                EdgeInsets.symmetric(horizontal: width(20)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: height(24)),
                                SimpleText(
                                  'Work Time',
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: blackColor3,
                                ),
                                SizedBox(height: height(6)),
                                SimpleText(
                                  'Monday - Friday, Morning 8AM - Night 8PM',
                                  fontWeight: FontWeight.w400,
                                  fontSize: 12,
                                  color: greyDarkColor3ab,
                                ),
                                SizedBox(height: height(24)),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    SimpleText(
                                      'Schedule',
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color: blackColor3,
                                    ),
                                    // SimpleText(
                                    //   'Feb 14',
                                    //   fontWeight: FontWeight.w400,
                                    //   fontSize: 12,
                                    //   color: greyDarkColor3ab,
                                    // ),
                                  ],
                                ),
                                SizedBox(height: height(10)),
                                DatePicker(
                                  DateTime.now(),
                                  height: height(98),
                                  initialSelectedDate: DateTime.now(),
                                  selectionColor: pinkColor2,
                                  selectedTextColor: whiteColor,
                                  monthTextStyle: GoogleFonts.poppins(
                                      textStyle: const TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12,
                                    color: greyDarkColor3ab,
                                  )),
                                  dateTextStyle: GoogleFonts.poppins(
                                      textStyle: const TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18,
                                    color: greyDarkColor,
                                  )),
                                  dayTextStyle: GoogleFonts.poppins(
                                      textStyle: const TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12,
                                    color: greyDarkColor3ab,
                                  )),
                                  onDateChange: (date) {
                                    // New date selected
                                    setState(() {
                                      _selectedValue = date;
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        _timePicker(),
                        SizedBox(
                          height: 10,
                        ),
                        Button('Book Now', onTap: () {
                          // print("TIME $finalTime");
                          if (finalTime != "Select Time") {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => BookingDetailsScreen(
                                        _cartList,
                                        widget.professionalUserModel,
                                        finalTime,
                                        _selectedValue.toString())));
                            // customerServices.bookProfessional(
                            //     context: context,
                            //     professionalId:
                            //         widget.professionalUserModel!.id.toString(),
                            //     bookingDate: _selectedValue.toString(),
                            //     price: widget.professionalUserModel!.price,
                            //     bookingTime:
                            //         finalTime != null ? finalTime : null);
                          } else {
                            EasyLoading.showInfo("Please Select Time");
                          }
                        }),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container doctorDetails(text, image, text2, text2Color) {
    return Container(
      height: height(65),
      width: width(98),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: greyLightColor4, width: 1.5),
          color: whiteColor),
      child: Padding(
        padding:
            EdgeInsets.only(left: width(10), top: height(6), bottom: height(6)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SimpleText(
              text,
              fontWeight: FontWeight.w400,
              fontSize: 10,
              color: blackColor3,
            ),
            Row(
              children: [
                SvgPicture.asset(image),
                SizedBox(width: width(5)),
                SimpleText(text2,
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    color: text2Color)
              ],
            )
          ],
        ),
      ),
    );
  }

  _timePicker() {
    return InkWell(
      onTap: () {
        // openTimePicker(context);
        BottomPicker.time(
          title: 'Set your next Booking time',
          titleStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: width(15),
            color: orangeColor,
          ),
          pickerTextStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: width(13),
            color: blackColor,
          ),
          onSubmit: (index) {
            print(index);
            List temp = index.toString().split(' ');
            String time = temp[1];
            print('>>>>$time');
            List temp2 = time.toString().split(':');
            setState(() {
              isSubmit = true;
              finalTime = '${temp2[0]} : ${temp2[1]}';
            });
          },
          onClose: () {
            print('Picker closed');
          },
          bottomPickerTheme: BOTTOM_PICKER_THEME.orange,
          use24hFormat: true,
        ).show(context);
      },
      child: Container(
        height: height(54),
        width: double.infinity,
        decoration: BoxDecoration(
            color: greyLightColor3,
            borderRadius: BorderRadius.circular(width(10)),
            border: Border.all(color: borderColor)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SimpleText(
              finalTime,
              fontWeight: FontWeight.w500,
              fontSize: 14,
              color: blackColor,
            ),
            SizedBox(width: width(10)),
            isSubmit ? SizedBox() : Icon(Icons.arrow_drop_down)
          ],
        ),
      ),
    );
  }

  ListView _buildGridView() {
    return ListView.builder(
      padding: const EdgeInsets.all(4.0),
      physics: ScrollPhysics(),
      itemCount: widget.professionalUserModel!.subservices!.length,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        var item = widget.professionalUserModel!.subservices![index];
        return Card(
            elevation: 4.0,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        item.name.toString(),
                        textAlign: TextAlign.start,
                      ),
                      Text(
                        "\$ ${item.price}",
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 18,
                          color: pinkColor,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Text(
                          "${item.description != null ? item.description : "Description not found."}",
                          softWrap: true,
                          maxLines: 3,
                          overflow: TextOverflow.visible,
                          style:
                              TextStyle(fontSize: 12, color: Colors.grey[600]),
                        ),
                      ),
                      GestureDetector(
                        child: (!_cartList.contains(item))
                            ? Icon(
                                Icons.add_circle,
                                color: Colors.green,
                              )
                            : Icon(
                                Icons.remove_circle,
                                color: Colors.red,
                              ),
                        onTap: () {
                          setState(() {
                            if (!_cartList.contains(item))
                              _cartList.add(item);
                            else
                              _cartList.remove(item);
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ));
      },
    );
  }
}
