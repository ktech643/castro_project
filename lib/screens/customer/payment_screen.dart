import 'package:castro_app/services/customer_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multi_formatter/formatters/credit_card_cvc_input_formatter.dart';
import 'package:flutter_multi_formatter/formatters/credit_card_expiration_input_formatter.dart';
import 'package:flutter_multi_formatter/formatters/credit_card_number_input_formatter.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../widgets/button.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/scalling.dart';
import '../../widgets/simple_text.dart';

class PaymentScreen extends StatefulWidget {
  final String? price;
  final String? professionalId;

  const PaymentScreen({Key? key, this.price, this.professionalId})
      : super(key: key);

  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController numberController = TextEditingController();
  TextEditingController monthController = TextEditingController();
  TextEditingController cvvController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  CustomerServices customerServices = CustomerServices();

  @override
  Widget build(BuildContext context) {
    init(context);
    return WillPopScope(
      onWillPop: () async {
        return Navigator.canPop(context);
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: whiteColor,
        appBar: AppBar(
          backgroundColor: whiteColor,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Padding(
              padding: EdgeInsets.all(20),
              child: SvgPicture.asset(
                'assets/back_button.svg',
                color: blackColor3,
              ),
            ),
          ),
          centerTitle: true,
          title: SimpleText('Payment Details', color: blackColor3),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: width(20)),
          child: SingleChildScrollView(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: height(37)),
              SimpleText('Payment Method',
                  fontWeight: FontWeight.w600,
                  fontSize: 13,
                  color: greyDarkColor),
              SizedBox(height: height(3)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  // Column(
                  //   children: [
                  //     Card(
                  //       elevation: 5,
                  //       child: Container(
                  //         height: height(48.44),
                  //         color: greyLightColor3,
                  //         child: Row(
                  //           children: [
                  //             Image.asset(
                  //               'assets/paypal.jpg',
                  //               fit: BoxFit.fill,
                  //               width: 40,
                  //               height: 29,
                  //             ),
                  //             SimpleText('Paypal',
                  //                 fontWeight: FontWeight.w500,
                  //                 fontSize: 12.4,
                  //                 color: blackColor3),
                  //             SizedBox(width: width(10))
                  //           ],
                  //         ),
                  //       ),
                  //     ),
                  //   ],
                  // ),
                  Column(
                    children: [
                      Card(
                        elevation: 5,
                        child: Container(
                          height: height(48.44),
                          color: greyLightColor3,
                          child: Padding(
                            padding:
                                EdgeInsets.symmetric(horizontal: width(10)),
                            child: Row(
                              children: [
                                Image.asset('assets/mastercard.png',
                                    width: width(28), height: height(18.7)),
                                SizedBox(width: width(6)),
                                SimpleText('Mastercard',
                                    fontWeight: FontWeight.w600,
                                    fontSize: 13,
                                    color: greyDarkColor),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Card(
                        elevation: 5,
                        child: Container(
                          height: height(48.44),
                          color: greyLightColor3,
                          child: Padding(
                            padding: EdgeInsets.all(width(13)),
                            child: Image.asset('assets/visa.png',
                                fit: BoxFit.fill),
                          ),
                          // child: Image.asset('assets/visa.png', fit: BoxFit.fill, width: width(44), height: height(18)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: height(26)),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SimpleText('Card number',
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: greyDarkColor),
                    Container(
                      height: height(44),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: greyLightColor3,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: width(3)),
                        child: TextFormField(
                          cursorColor: greyDarkColor,
                          controller: numberController,
                          textAlignVertical: TextAlignVertical.center,
                          decoration: InputDecoration(
                            prefixIcon: Container(
                              height: height(19),
                              width: width(30),
                              child: Padding(
                                padding: EdgeInsets.all(width(7)),
                                child: Image.asset('assets/mastercard.png'),
                              ),
                            ),
                            border: InputBorder.none,
                            hintText: '**** **** **** ****',
                            errorStyle: const TextStyle(
                              color: orangeColor,
                            ),
                          ),
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(19),
                            CreditCardNumberInputFormatter(),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: height(20)),
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SimpleText('Valid Until',
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  color: greyDarkColor),
                              Container(
                                  height: height(44),
                                  width: double.infinity,
                                  alignment: Alignment.centerLeft,
                                  decoration: BoxDecoration(
                                    color: greyLightColor3,
                                    borderRadius: BorderRadius.circular(6),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: width(8)),
                                    child: TextFormField(
                                      // textAlign: TextAlign.center,
                                      cursorColor: greyDarkColor,
                                      controller: monthController,
                                      decoration: InputDecoration(
                                        // contentPadding: EdgeInsets.only(bottom: height(5)),
                                        border: InputBorder.none,
                                        hintText: 'Month/Year',
                                        hintStyle: GoogleFonts.nunito(
                                            textStyle: TextStyle(
                                                fontSize: width(13),
                                                color: greyDarkColor,
                                                fontWeight: FontWeight.w600)),
                                        errorStyle: const TextStyle(
                                          color: orangeColor,
                                        ),
                                      ),
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        CreditCardExpirationDateFormatter(),
                                      ],
                                    ),
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(width: height(20)),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SimpleText('CVV',
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  color: greyDarkColor),
                              Container(
                                  height: height(44),
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: greyLightColor3,
                                    borderRadius: BorderRadius.circular(6),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: width(8)),
                                    child: TextFormField(
                                      cursorColor: greyDarkColor,
                                      controller: cvvController,
                                      textAlignVertical:
                                          TextAlignVertical.center,
                                      decoration: const InputDecoration(
                                        border: InputBorder.none,
                                        hintText: '***',
                                        errorStyle: TextStyle(
                                          color: orangeColor,
                                        ),
                                      ),
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        CreditCardCvcInputFormatter(),
                                      ],
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: height(20)),
                    SimpleText('Card Holder',
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: greyDarkColor),
                    Container(
                        height: height(44),
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: greyLightColor3,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: width(8)),
                          child: TextFormField(
                            cursorColor: greyDarkColor,
                            controller: nameController,
                            decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.only(bottom: height(5)),
                              border: InputBorder.none,
                              hintText: 'Your Name',
                              hintStyle: GoogleFonts.nunito(
                                  textStyle: TextStyle(
                                      fontSize: width(13),
                                      color: greyDarkColor,
                                      fontWeight: FontWeight.w600)),
                              errorStyle: const TextStyle(
                                color: orangeColor,
                              ),
                            ),
                            keyboardType: TextInputType.name,
                          ),
                        )),
                  ],
                ),
              ),
              SizedBox(height: height(98)),
              Button('Pay \$${widget.price != null ? widget.price : "0"}',
                  onTap: () {
                FocusScope.of(context).unfocus();
                if (_formKey.currentState!.validate()) {
                  String month = monthController.text;
                  var result = month.split("/");
                  customerServices.paymentStripe(
                      context: context,
                      amount: widget.price,
                      card_month: result[0],
                      card_year: result[1],
                      card_cvv: cvvController.text,
                      card_holder: nameController.text,
                      professionalId: widget.professionalId,
                      card_no: nameController.text);
                } else {}
              }),
            ],
          )),
        ),
      ),
    );
  }
}
