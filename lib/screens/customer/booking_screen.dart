import 'package:castro_app/models/customer/booking_model.dart';
import 'package:castro_app/screens/core/bottom_navigation_bar.dart';
import 'package:castro_app/services/customer_services.dart';
import 'package:castro_app/utilis/constants.dart';
import 'package:castro_app/widgets/willPopScope.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rating_dialog/rating_dialog.dart';
import '../../widgets/primaryColor.dart';
import '../../widgets/scalling.dart';
import '../../widgets/simple_text.dart';

class BookingScreen extends StatefulWidget {
  const BookingScreen({Key? key}) : super(key: key);

  @override
  _BookingScreenState createState() => _BookingScreenState();
}

class _BookingScreenState extends State<BookingScreen> {
  CustomerServices customerServices = CustomerServices();
  List<BookingModel> bookingModel = [];

  @override
  Widget build(BuildContext context) {
    init(context);
    return WillPopScope(
      onWillPop: () async {
        return willPopScope(context, BottomNavBar());
      },
      child: Scaffold(
        // backgroundColor: whiteColor,
        appBar: AppBar(
          backgroundColor: whiteColor,
          // leading: InkWell(
          //   onTap: () {
          //     Navigator.pop(context);
          //   },
          //   child: Padding(
          //     padding: EdgeInsets.all(20),
          //     child: SvgPicture.asset(
          //       'assets/back_button.svg',
          //       color: blackColor3,
          //     ),
          //   ),
          // ),
          centerTitle: true,
          title: SimpleText('Bookings', color: blackColor3),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                child: FutureBuilder(
              future: customerServices.getBookings(),
              builder: (context, snapshot) {
                print("SNAPSHOT DATA ${snapshot.data}");
                if (snapshot.hasData) {
                  bookingModel = snapshot.data as List<BookingModel>;
                  return bookingModel.isNotEmpty
                      ? ListView.separated(
                          itemCount: bookingModel.length,
                          separatorBuilder: (BuildContext context, int index) =>
                              SizedBox(height: height(16)),
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              onTap: () {
                                // Navigator.of(context).push(MaterialPageRoute(
                                //     builder: (context) =>
                                //         const BookingDetailsScreen()));
                              },
                              child: Column(
                                children: [
                                  if (index == 0) SizedBox(height: height(24)),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: width(20)),
                                    child: Card(
                                      color: whiteColor,
                                      elevation: 3,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(width(10)),
                                      ),
                                      child: Container(
                                        height: height(140),
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: width(20)),
                                          child: Center(
                                            child: Container(
                                              height: height(108),
                                              child: Row(
                                                children: [
                                                  bookingModel[index]
                                                              .professional_user!
                                                              .image !=
                                                          null
                                                      ? Container(
                                                          height: height(89.06),
                                                          width: width(85.76),
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          width(
                                                                              10)),
                                                              color: whiteColor,
                                                              image: DecorationImage(
                                                                  image: NetworkImage(Constants
                                                                          .IMAGE_URL +
                                                                      bookingModel[
                                                                              index]
                                                                          .professional_user!
                                                                          .image
                                                                          .toString()),
                                                                  fit: BoxFit
                                                                      .fill)),
                                                        )
                                                      : Container(
                                                          height: height(89.06),
                                                          width: width(85.76),
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          width(
                                                                              10)),
                                                              color: whiteColor,
                                                              image: const DecorationImage(
                                                                  image: AssetImage(
                                                                      "assets/profile.png"),
                                                                  fit: BoxFit
                                                                      .fill)),
                                                        ),
                                                  Expanded(
                                                    child: Padding(
                                                      padding: EdgeInsets.only(
                                                          left: width(10)),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                children: [
                                                                  SimpleText(
                                                                    bookingModel[
                                                                            index]
                                                                        .professional_user!
                                                                        .name,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600,
                                                                    fontSize:
                                                                        14,
                                                                    color:
                                                                        blackColor3,
                                                                  ),
                                                                  SimpleText(
                                                                    '\$${bookingModel[index].total_price != null ? bookingModel[index].total_price : "0"}.00',
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600,
                                                                    fontSize:
                                                                        14,
                                                                    color:
                                                                        pinkColor,
                                                                  ),
                                                                ],
                                                              ),
                                                              SimpleText(
                                                                "${bookingModel[index].professional_user!.speciality != null ? bookingModel[index].professional_user!.speciality!.name : "Not found"}",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 12,
                                                                color:
                                                                    greyIconColor,
                                                              ),
                                                            ],
                                                          ),
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              SimpleText(
                                                                bookingModel[
                                                                        index]
                                                                    .booking_date,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 12,
                                                                color:
                                                                    blackColor3,
                                                              ),
                                                              Row(
                                                                children: [
                                                                  SimpleText(
                                                                    bookingModel[
                                                                            index]
                                                                        .status,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600,
                                                                    fontSize:
                                                                        12,
                                                                    color:
                                                                        pinkColor,
                                                                  ),
                                                                  Spacer(),
                                                                  bookingModel[index]
                                                                              .status ==
                                                                          "completed"
                                                                      ? GestureDetector(
                                                                          onTap:
                                                                              () {
                                                                            showDialog(
                                                                              context: context,
                                                                              barrierDismissible: true,
                                                                              // set to false if you want to force a rating
                                                                              builder: (context) => RatingDialog(
                                                                                initialRating: 5.0,
                                                                                starSize: 30,
                                                                                // your app's name?
                                                                                title: Text(
                                                                                  'Rating',
                                                                                  textAlign: TextAlign.center,
                                                                                  style: const TextStyle(
                                                                                    fontSize: 25,
                                                                                    fontWeight: FontWeight.bold,
                                                                                  ),
                                                                                ),
                                                                                // encourage your user to leave a high rating?
                                                                                message: Text(
                                                                                  'Please give your valuable feedback.',
                                                                                  textAlign: TextAlign.center,
                                                                                  style: const TextStyle(fontSize: 15),
                                                                                ),
                                                                                submitButtonTextStyle: TextStyle(color: Colors.green, fontSize: 20, fontWeight: FontWeight.bold),
                                                                                // your app's logo?
                                                                                submitButtonText: 'Submit',
                                                                                commentHint: 'Write your comments',
                                                                                onCancelled: () => print('cancelled'),
                                                                                onSubmitted: (response) {
                                                                                  customerServices.submitReview(context: context, rating: response.rating.toString(), review: response.comment.toString(), professionalId: bookingModel[index].professional_user_id);

                                                                                  // // TODO: add your own logic
                                                                                  // if (response.rating < 3.0) {
                                                                                  //   // send their comments to your email or anywhere you wish
                                                                                  //   // ask the user to contact you instead of leaving a bad review
                                                                                  // } else {}
                                                                                },
                                                                              ),
                                                                            );
                                                                          },
                                                                          child:
                                                                              Text(
                                                                            "Give rating",
                                                                            style:
                                                                                TextStyle(color: Colors.green),
                                                                          ),
                                                                        )
                                                                      : SizedBox()
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  if (index == 9) SizedBox(height: height(20)),
                                ],
                              ),
                            );
                          },
                        )
                      : Center(child: Text("No booking found"));
                }
                return Center(child: Text("No booking found"));
              },
            )),
          ],
        ),
      ),
    );
  }
}
