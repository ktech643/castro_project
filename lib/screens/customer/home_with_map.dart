import 'dart:async';

import 'package:castro_app/widgets/specialist_container.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../utilis/secure_storage.dart';
import '../../widgets/scalling.dart';
import 'doctor_details.dart';

class HomeMap extends StatefulWidget {
  const HomeMap({Key? key}) : super(key: key);

  @override
  _HomeMapState createState() => _HomeMapState();
}

class _HomeMapState extends State<HomeMap> {
  Completer<GoogleMapController> _mapController = Completer();
  LocationPermission? permission;
  GoogleMapController? _googleMapController;
  LatLng currentPostion = LatLng(37.42796133580664, -122.085749655962);
  List<Marker> _markers = [];
  String? _mapStyle;
  bool loading = false;
  SecureStorage secureStorage = SecureStorage();
  String? name;
  String? email;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUserLocation();
  }

  void _getUserLocation() async {
    bool serviceEnabled;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => Dialog(
          child: Container(
            height: 250,
            child: Padding(
              padding: EdgeInsets.all(12.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.location_on,
                    color: Colors.pink,
                    size: 28,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Use your location',
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: "Segoe UI",
                        fontWeight: FontWeight.w800,
                        fontSize: 18),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text:
                              'Castro wants to get your location so you can find your nearby Specialist. For furthur info please check our ',
                          style: TextStyle(fontSize: 16),
                        ),
                        TextSpan(
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              // Helper.launchURL();
                            },
                          text: "Privacy policy",
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.pink,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 12, vertical: 12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "No thanks",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.pink,
                                fontFamily: "Segoe UI"),
                          ),
                        ),
                        Spacer(),
                        GestureDetector(
                          onTap: () async {
                            Navigator.pop(context);
                            permission = await Geolocator.requestPermission();
                            var position = await GeolocatorPlatform.instance
                                .getCurrentPosition();
                            setState(() {
                              currentPostion =
                                  LatLng(position.latitude, position.longitude);
                              loading = true;
                            });
                          },
                          child: Text(
                            "Turn on",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.pink,
                                fontFamily: "Segoe UI"),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    } else {
      var position = await GeolocatorPlatform.instance.getCurrentPosition();
      setState(() {
        currentPostion = LatLng(position.latitude, position.longitude);
        loading = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    init(context);
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: height(10)),
          SizedBox(
            height: 200,
            child: loading
                ? GoogleMap(
                    mapType: MapType.normal,
                    initialCameraPosition: CameraPosition(
                      target: currentPostion,
                      zoom: 14,
                    ),
                    onMapCreated: (GoogleMapController controller) => {
                      setState(() {
                        _googleMapController = controller;
                        _mapController.complete(controller);
                        _googleMapController?.setMapStyle(_mapStyle);
                      })
                    },
                    markers: Set.of(_markers),
                    gestureRecognizers: Set()
                      ..add(Factory<PanGestureRecognizer>(
                          () => PanGestureRecognizer())),
                    rotateGesturesEnabled: false,
                    compassEnabled: false,
                    myLocationEnabled: true,
                    myLocationButtonEnabled: true,
                    zoomGesturesEnabled: false,
                    zoomControlsEnabled: true,
                  )
                : Container(
                    child: Center(
                      child: Text("Check location permission."),
                    ),
                  ),
            // child: GoogleMap(
            //   mapType: MapType.normal,
            //   initialCameraPosition: _kGooglePlex,
            //   onMapCreated: (GoogleMapController controller) {
            //     _mapController.complete(controller);
            //   },
            // ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
            child: Column(
              children: [
                SpecialistContainer(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => DoctorDetails()));
                  },
                  name: "Dr. John Doe",
                  address: "Cuesta Park - now",
                  time: "4 min",
                  image: SizedBox(),
                ),
                SizedBox(
                  height: 20,
                ),
                SpecialistContainer(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => DoctorDetails()));
                    },
                    name: "Dr. John Doe",
                    image: SizedBox(),
                    address: "Cuesta Park - now",
                    time: "4 min"),
                SizedBox(
                  height: 20,
                ),
                // SpecialistContainer(
                //     onTap: () {
                //       Navigator.of(context).push(MaterialPageRoute(
                //           builder: (context) => const DoctorDetails()));
                //     },
                //     name: "Dr. John Doe",
                //     address: "Cuesta Park - now",
                //     time: "4 min"),
                // SizedBox(
                //   height: 20,
                // ),
                // SpecialistContainer(
                //     onTap: () {
                //       Navigator.of(context).push(MaterialPageRoute(
                //           builder: (context) => const DoctorDetails()));
                //     },
                //     name: "Dr. John Doe",
                //     address: "Cuesta Park - now",
                //     time: "4 min"),
              ],
            ),
          )
        ],
      ),
    );
  }
}
