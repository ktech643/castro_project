import 'dart:convert';
import 'package:castro_app/models/notification_model.dart';
import 'package:castro_app/utilis/constants.dart';
import 'package:http/http.dart' as http;
import '../utilis/secure_storage.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class FirebaseServices {
  SecureStorage storage = SecureStorage();

  Future<void> updateFcmToken({required String fcm_Token}) async {
    try {
      String? token = await storage.readStore("token");
      var url = Uri.parse('${Constants.BASE_URL}update-fcm');
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {
        "fcm_token": fcm_Token
      });
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {}
      return null;
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

  Future<List<NotificationModel>> getNotification() async {
    List<NotificationModel> notificationList = [];
    try {
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      var url = Uri.parse('${Constants.BASE_URL}get-notifications');
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {});
      var responseBody = json.decode(response.body);
      print(responseBody);

      if (response.statusCode == 200) {
        notificationList = responseBody['notifications']
            .map<NotificationModel>((notificationModel) =>
                NotificationModel.fromJson(notificationModel))
            .toList();
      } else {
        EasyLoading.showError(responseBody['message']);
      }
      EasyLoading.dismiss();
      return notificationList;
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
    return notificationList;
  }
}
