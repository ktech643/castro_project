import 'dart:convert';
import 'package:castro_app/models/professional/appointment_model.dart';
import 'package:castro_app/models/professional/professional_user_model.dart';
import 'package:castro_app/utilis/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import '../models/professional/specialities_list.dart';
import '../screens/core/professional_bottom_navigation_bar.dart';
import '../utilis/secure_storage.dart';

class ProfessionalServices {
  SecureStorage storage = SecureStorage();

  //------------------GET SPECIALITIES API------------------
  Future<List<SpecialtiesModel>> getSpecialities(
      {required bool addlist}) async {
    List<SpecialtiesModel> specialitiesModel = [];
    try {
      var url = Uri.parse('${Constants.BASE_URL}get-specialities');
      var response =
          await http.post(url, headers: <String, String>{}, body: {});
      var responseBody = json.decode(response.body);

      if (response.statusCode == 200) {
        specialitiesModel = responseBody["specialities"]
            .map<SpecialtiesModel>(
                (language) => SpecialtiesModel.fromJson(language))
            .toList();
        // EasyLoading.showSuccess(responseBody['message']);
        if (addlist == true) {
          specialitiesModel.insert(
              0, SpecialtiesModel(id: 00, name: "Select your speciality"));
        }
        return specialitiesModel;
      } else {
        EasyLoading.showError(responseBody['message']);
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
    // EasyLoading.show(status: "Please wait....");

    return specialitiesModel;
  }

  //------------------GET PROFESSIONAL USER BY ID API------------------

  Future<List<ProfessionalUserModel>> getProfessionalUserById(
      {int? speciality_id}) async {
    List<ProfessionalUserModel> professionalUserList = [];
    try {
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      var url = Uri.parse('${Constants.BASE_URL}get-professional-user');
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {
        "speciality_id": speciality_id.toString()
      });
      var responseBody = json.decode(response.body);

      if (response.statusCode == 200) {
        print(response.body);
        professionalUserList = responseBody["professional_users"]
            .map<ProfessionalUserModel>((professionalModel) =>
                ProfessionalUserModel.fromJson(professionalModel))
            .toList();
      } else {
        EasyLoading.showError(responseBody['message']);
      }
      EasyLoading.dismiss();
      return professionalUserList;
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
    return professionalUserList;
  }

  //------------------GET PROFESSIONAL USER BY NAME API------------------

  Future<List<ProfessionalUserModel>> getProfessionalUserByName(
      {String? name}) async {
    List<ProfessionalUserModel> professionalUserList = [];
    try {
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      var url = Uri.parse('${Constants.BASE_URL}search');
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {
        "search_string": name
      });
      var responseBody = json.decode(response.body);

      if (response.statusCode == 200) {
        professionalUserList = responseBody["professional_users"]
            .map<ProfessionalUserModel>((professionalModel) =>
                ProfessionalUserModel.fromJson(professionalModel))
            .toList();
      } else {
        EasyLoading.showError(responseBody['message']);
      }
      EasyLoading.dismiss();
      return professionalUserList;
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
    return professionalUserList;
  }

  //------------------UPDATE PROFESSIONAL PROFILE API------------------
  Future<void> updateProfessionalProfile({
    String? name,
    String? zip_code,
    String? phone,
    String? image,
    String? address,
    String? experience,
    String? specialityId,
    String? price,
    required BuildContext context,
  }) async {
    try {
      EasyLoading.show(status: 'loading...');
      String? jwt = await storage.readStore('token');
      jwt = jwt ?? '';
      Map<String, String> headers = {
        'Authorization': 'Bearer ' + jwt,
      };
      var request = http.MultipartRequest(
          'POST', Uri.parse("${Constants.BASE_URL}" + "update-profile"));
      request.headers.addAll(headers);
      if (image != null) {
        request.files.add(await http.MultipartFile.fromPath('image', image));
      }
      if (name != null) {
        request.fields['name'] = name;
      }
      if (zip_code != null) {
        request.fields['zip_code'] = zip_code;
      }
      if (phone != null) {
        request.fields['phone'] = phone;
      }
      if (address != null) {
        request.fields['address'] = address;
      }
      if (experience != null) {
        request.fields['experience'] = experience;
      }
      if (price != null) {
        request.fields['price'] = price;
      }
      if (specialityId != "Select your speciality") {
        request.fields['speciality_id'] = specialityId.toString();
      }
      var res = await request.send();
      var responed = await http.Response.fromStream(res);
      var responseData = json.decode(responed.body);
      if (responseData['code'] == 200) {
        EasyLoading.dismiss(animation: true);
        EasyLoading.showSuccess(
            responseData['message'] ?? "Saved Successfully");
      } else {
        EasyLoading.dismiss(animation: true);
        EasyLoading.showError(
            responseData['message'] ?? "Something went wrong");
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

  //------------------GET APPOINTMENTS API------------------
  Future<List<AppointmentModel>> getAppoinments() async {
    List<AppointmentModel> appointmentList = [];
    try {
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      var url = Uri.parse('${Constants.BASE_URL}my-appointments');
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {});
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        appointmentList = responseBody
            .map<AppointmentModel>((appointmentModel) =>
                AppointmentModel.fromJson(appointmentModel))
            .toList();
      } else {
        EasyLoading.showError(responseBody['message']);
      }
      EasyLoading.dismiss();
      return appointmentList;
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
    return appointmentList;
  }

//------------------ACCEPT APPOINTMENT API------------------
  Future<void> acceptAppointment(
      {int? book_specialist_id, required BuildContext context}) async {
    try {
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      var url = Uri.parse('${Constants.BASE_URL}accept-request');
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {
        "book_specialist_id": book_specialist_id.toString()
      });
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.showSuccess(responseBody['message']);
        Future.delayed(Duration(seconds: 1), () {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => ProfessionalBottomNavBar()));
        });
      } else {
        EasyLoading.showError(responseBody['message']);
      }
      EasyLoading.dismiss();
      return null;
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

  //------------------ REJECT APPOINTMENTS API------------------
  Future<void> rejectAppointment(
      {int? book_specialist_id, required BuildContext context}) async {
    try {
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      var url = Uri.parse('${Constants.BASE_URL}reject-request');
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {
        "book_specialist_id": book_specialist_id.toString()
      });
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.showSuccess(responseBody['message']);
        Future.delayed(Duration(seconds: 1), () {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => ProfessionalBottomNavBar()));
        });
      } else {
        EasyLoading.showError(responseBody['message']);
      }
      EasyLoading.dismiss();
      return null;
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

//------------------COMPLETE APPOINTMENTS API------------------
  Future<void> completeAppoinment(
      {int? book_specialist_id, required BuildContext context}) async {
    try {
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      var url = Uri.parse('${Constants.BASE_URL}complete-appointment');
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {
        "book_specialist_id": book_specialist_id.toString()
      });
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.showSuccess(responseBody['message']);
        Future.delayed(Duration(seconds: 1), () {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => ProfessionalBottomNavBar()));
        });
      } else {
        EasyLoading.showError(responseBody['message']);
      }
      EasyLoading.dismiss();
      return null;
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

//------------------CREATE SUB SERVICES API------------------
  addSubServices(
      {required String name,
      required price,
      String? description,
      required BuildContext context}) async {
    try {
      EasyLoading.show(status: "Please wait....");
      var url = Uri.parse('${Constants.BASE_URL}add-sub-service');
      String? token = await storage.readStore("token");
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {
        'name': name,
        'price': price,
        'description': description,
      });
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        Navigator.pop(context);
        Future.delayed(Duration.zero, () {
          EasyLoading.showSuccess(responseBody['message']);
        });
      } else {
        EasyLoading.showError(responseBody['message']);
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

//------------------UPDATE SUB SERVICES API------------------
  updateSubServices(
      {required String name,
      required price,
      String? description,
      String? subservice_id,
      required BuildContext context}) async {
    try {
      EasyLoading.show(status: "Please wait....");
      var url = Uri.parse('${Constants.BASE_URL}update-sub-service');
      String? token = await storage.readStore("token");
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {
        'name': name,
        'subservice_id': subservice_id,
        'price': price,
        'description': description,
      });
      var responseBody = json.decode(response.body);
      print(
          "---------------------=================UPDATE=======${responseBody}====--");
      if (response.statusCode == 200) {
        Navigator.pop(context);
        Future.delayed(Duration.zero, () {
          EasyLoading.showSuccess(responseBody['message']);
        });
      } else {
        EasyLoading.showError(responseBody['message']);
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

//------------------GET SUB_SERVICES API------------------
  Future<List<SubServicesModel>> getSubServices() async {
    List<SubServicesModel> subServicesList = [];
    try {
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      var url = Uri.parse('${Constants.BASE_URL}get-sub-service');
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {});
      var responseBody = json.decode(response.body);
      print(responseBody);

      if (response.statusCode == 200) {
        subServicesList = responseBody['sub-services']
            .map<SubServicesModel>((notificationModel) =>
                SubServicesModel.fromJson(notificationModel))
            .toList();
      } else {
        EasyLoading.showError(responseBody['message']);
      }
      EasyLoading.dismiss();
      return subServicesList;
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
    return subServicesList;
  }

  //-----------------------DELETE SUB SERVICES--------------
  Future<void> deleteSubServices(
      {required String id, required BuildContext context}) async {
    try {
      EasyLoading.show(status: "Please wait....");
      var url = Uri.parse('${Constants.BASE_URL}delete-sub-service');
      String? token = await storage.readStore("token");
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {
        'subservice_id': id,
      });
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        Future.delayed(Duration.zero, () {
          EasyLoading.showSuccess(responseBody['0']);
        });
      } else {
        EasyLoading.showError(responseBody['message']);
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }
}
