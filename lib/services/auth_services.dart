import 'dart:convert';

import 'package:castro_app/utilis/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;

import '../screens/core/bottom_navigation_bar.dart';
import '../screens/core/professional_bottom_navigation_bar.dart';
import '../utilis/secure_storage.dart';

class AuthService {
  SecureStorage storage = SecureStorage();

  //------------------SIGN IN API-----------------
  signIn(
      {required String email,
      required String password,
      required BuildContext context}) async {
    try {
      EasyLoading.show(status: "Please wait....");
      var url = Uri.parse('${Constants.BASE_URL}sign_in');
      var response = await http.post(url,
          headers: <String, String>{},
          body: {'email': email, 'password': password});
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        storage.setStore("token", responseBody['data']['token']);
        storage.setStore("id", responseBody['data']['id'].toString());
        storage.setStore("role", responseBody['data']['register_as']);
        storage.setStore("name", responseBody['data']['name']);
        storage.setStore("price", responseBody['data']['price']);
        storage.setStore("email", responseBody['data']['email']);
        if (responseBody['data']['register_as'] == "customer") {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => const BottomNavBar()));
        } else if (responseBody['data']['register_as'] == "professional") {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => ProfessionalBottomNavBar()));
        }
        EasyLoading.showSuccess(responseBody['message']);
      } else {
        EasyLoading.showError(responseBody['message']);
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

//------------------CUSTOMER SIGN UP API------------------
  customerSignUp(
      {required String email,
      required String name,
      String? phoneNo,
      String? gender,
      String? dob,
      required String password,
      required BuildContext context}) async {
    try {
      EasyLoading.show(status: "Please wait....");
      var url = Uri.parse('${Constants.BASE_URL}sign_up');
      var response = await http.post(url, headers: <String, String>{}, body: {
        'email': email,
        'password': password,
        'register_as': 'customer',
        'name': name,
        'phone': phoneNo,
        'gender': gender,
        'dob': dob,
      });
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        storage.setStore("token", responseBody['data']['token']);
        storage.setStore("id", responseBody['data']['id'].toString());
        storage.setStore("role", responseBody['data']['register_as']);
        storage.setStore("name", responseBody['data']['name']);
        storage.setStore("email", responseBody['data']['email']);
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => const BottomNavBar()));
        EasyLoading.showSuccess(responseBody['message']);
      } else {
        EasyLoading.showError(responseBody['message']);
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

//------------------PROFESSIONAL SIGN UP API------------------
  professionalSignUp(
      {required String email,
      required String name,
      required String experience,
      required String price,
      String? phoneNo,
      String? zipcode,
      String? address,
      String? speciality,
      required String password,
      required String longitude,
      required String latitude,
      required BuildContext context}) async {
    try {
      EasyLoading.show(status: "Please wait....");
      var url = Uri.parse('${Constants.BASE_URL}sign_up');
      var response = await http.post(url, headers: <String, String>{}, body: {
        'email': email,
        'password': password,
        'register_as': 'professional',
        'name': name,
        'phone': phoneNo,
        'speciality_id': speciality,
        'experience': experience,
        'price': price,
        'address': address,
        'zip_code': zipcode,
        'latitude': latitude,
        'longitude': longitude,
      });
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        storage.setStore("token", responseBody['data']['token']);
        storage.setStore("id", responseBody['data']['id'].toString());
        storage.setStore("role", responseBody['data']['register_as']);
        storage.setStore("name", responseBody['data']['name']);
        storage.setStore("email", responseBody['data']['email']);
        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => ProfessionalBottomNavBar()));
        EasyLoading.showSuccess(responseBody['message']);
      } else {
        EasyLoading.showError(responseBody['message']);
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

//------------------FORGET PASSWORD API------------------
  forgetPassword({required String email, required BuildContext context}) async {
    try {
      EasyLoading.show(status: "Please wait....");
      var url = Uri.parse('${Constants.BASE_URL}forgot_password');
      var response = await http.post(url, headers: <String, String>{}, body: {
        'email': email,
      });
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.showSuccess(responseBody['message']);
        Future.delayed(Duration.zero, () {
          Navigator.pop(context);
        });
      } else {
        EasyLoading.showError(responseBody['message']);
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

//------------------SEND OTP API------------------
  sendOTP(
      {required String email,
      required String otp,
      required BuildContext context}) async {
    try {
      EasyLoading.show(status: "Please wait....");
      var url = Uri.parse('${Constants.BASE_URL}verify_otp');
      var response = await http.post(url,
          headers: <String, String>{}, body: {'email': email, 'otp': otp});
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.showSuccess(responseBody['message']);
      } else {
        EasyLoading.showError(responseBody['message']);
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }
}
