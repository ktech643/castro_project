import 'package:castro_app/models/customer/booking_model.dart';
import '../models/professional/professional_user_model.dart';
import 'dart:convert';
import 'package:castro_app/utilis/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import '../screens/core/bottom_navigation_bar.dart';
import '../screens/customer/payment_screen.dart';
import '../utilis/secure_storage.dart';

class CustomerServices {
  SecureStorage storage = SecureStorage();

//------------------GET USER PROFILE API------------------
  Future<ProfessionalUserModel?> getUserProfile() async {
    try {
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      var url = Uri.parse('${Constants.BASE_URL}my-profile');
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {});
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.dismiss();
        ProfessionalUserModel professionalUserProfile =
            ProfessionalUserModel.fromJson(responseBody["my_profile"]);
        return professionalUserProfile;
      } else {
        EasyLoading.showError(responseBody['message']);
      }
      EasyLoading.dismiss();
      return null;
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
    return null;
  }

//------------------UPDATE CUSTOMER PROFILE API------------------
  Future<void> updateCustomerProfile({
    String? name,
    String? dob,
    String? phone,
    String? image,
    required BuildContext context,
  }) async {
    try {
      EasyLoading.show(status: 'loading...');
      String? jwt = await storage.readStore('token');
      jwt = jwt ?? '';
      Map<String, String> headers = {
        'Authorization': 'Bearer ' + jwt,
      };
      var request = http.MultipartRequest(
          'POST', Uri.parse("${Constants.BASE_URL}" + "update-profile"));
      request.headers.addAll(headers);
      if (image != null) {
        request.files.add(await http.MultipartFile.fromPath('image', image));
      }
      if (name != null) {
        request.fields['name'] = name;
      }
      if (dob != null) {
        request.fields['dob'] = dob;
      }
      if (phone != null) {
        request.fields['phone'] = phone;
      }
      var res = await request.send();
      var responed = await http.Response.fromStream(res);
      var responseData = json.decode(responed.body);
      if (responseData['code'] == 200) {
        EasyLoading.dismiss(animation: true);
        EasyLoading.showSuccess(
            responseData['message'] ?? "Saved Successfully");
      } else {
        EasyLoading.dismiss(animation: true);
        EasyLoading.showError(
            responseData['message'] ?? "Something went wrong");
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

//------------------BOOK PROFESSIONAL API------------------
  bookProfessional(
      {String? professionalId,
      String? bookingTime,
      String? bookingDate,
      String? price,
      List<SubServicesModel>? subServices,
      required BuildContext context}) async {
    try {
      Map<String, String> body = {};
      int index = 0;
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      String? id = await storage.readStore("id");
      var url = Uri.parse('${Constants.BASE_URL}book-professional');
      subServices?.forEach((services) {
        body['sub_services[$index]'] = services.id.toString();
        index++;
      });
      if (id != null) body['customer_id'] = id;
      if (professionalId != null) body['professional_user_id'] = professionalId;
      if (bookingDate != null) body['booking_date'] = bookingDate;
      if (bookingTime != null) body['booking_time'] = bookingTime;
      var response = await http.post(url,
          headers: <String, String>{
            'Authorization': 'Bearer $token',
          },
          body: body);
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.showSuccess(responseBody['message']);
        Future.delayed(const Duration(seconds: 1), () {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => PaymentScreen(
                    price: price,
                    professionalId: professionalId,
                  )));
        });
      } else {
        EasyLoading.showError(responseBody['message']);
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

//------------------GET BOOKINGS API------------------
  Future<List<BookingModel>> getBookings() async {
    List<BookingModel> bookingList = [];
    try {
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      var url = Uri.parse('${Constants.BASE_URL}my-bookings');
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {});
      var responseBody = json.decode(response.body);
      print("responseBody      $responseBody");
      print("Status Code      ${response.statusCode}");
      if (response.statusCode == 200) {
        print("in 200");
        bookingList = responseBody
            .map<BookingModel>(
                (bookingModel) => BookingModel.fromJson(bookingModel))
            .toList();

        EasyLoading.dismiss();
      } else {
        EasyLoading.showError(responseBody['message']);
        EasyLoading.dismiss();
      }
      EasyLoading.dismiss();
      return bookingList;
    } catch (e) {
      print("Exception $e");
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
    return bookingList;
  }

//------------------PAYMENT API------------------
  paymentStripe(
      {String? professionalId,
      String? amount,
      String? card_holder,
      String? card_no,
      String? card_month,
      String? card_year,
      String? card_cvv,
      required BuildContext context}) async {
    try {
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      String? id = await storage.readStore("id");
      var url = Uri.parse('${Constants.BASE_URL}pay-against-booking');
      var response = await http.post(url, headers: <String, String>{
        'Authorization': 'Bearer $token',
      }, body: {
        'book_specialist_id': professionalId,
        'amount': amount,
        'card_no': card_no,
        'card_holder': card_holder,
        'card_month': card_month,
        'card_year': card_year,
        'card_cvv': card_cvv,
      });
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.showSuccess(responseBody['message']);
        Future.delayed(const Duration(seconds: 1), () {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => const BottomNavBar()));
        });
      } else {
        EasyLoading.showError(responseBody['message']);
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }

//-----------------Submit FeedBack----------------
  submitReview(
      {String? professionalId,
      String? review,
      String? rating,
      required BuildContext context}) async {
    try {
      Map<String, String> body = {};
      int index = 0;
      EasyLoading.show(status: "Please wait....");
      String? token = await storage.readStore("token");
      var url = Uri.parse('${Constants.BASE_URL}rating-review');
      if (professionalId != null) body['profesional_user_id'] = professionalId;
      if (rating != null) body['rating'] = rating;
      if (review != null) body['review'] = review;
      var response = await http.post(url,
          headers: <String, String>{
            'Authorization': 'Bearer $token',
          },
          body: body);
      var responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.showSuccess(responseBody['message']);
        Future.delayed(const Duration(seconds: 1), () {});
      } else {
        EasyLoading.showError(responseBody['message']);
      }
    } catch (e) {
      EasyLoading.dismiss();
      EasyLoading.showError(e.toString());
    }
  }
}
