import 'package:url_launcher/url_launcher.dart';

class Validator {
  final Uri url = Uri.parse('https://sites.google.com/view/thecastro/home');
  final Uri url2 =
  Uri.parse('https://sites.google.com/view/castroappterms/home');
  static bool isName(String text) {
    return text
        .contains(new RegExp(r"^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$"));
  }

  static bool validateString(String text) {
    return RegExp(
            r'^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$')
        .hasMatch(text);
  }

  static String? validateConfirmPassword(String? text, String? text1) {
    String? message;
    if (text == text1) {
      return null;
    } else {
      message = 'Your password doesn\'t match';
    }
    return message;
  }

  static String? validateEmail(String? text) {
    String? message;
    if (text!.isEmpty) {
      message = 'Please enter email';
    }
    if (!Helper.emailRegExp(text)) {
      message = 'Please enter valid email';
    }

    return message;
  }

  static String? validatePassword(String? text,
      {validateNumberCharacters = true, validateSpecial = false}) {
    String? message;
    if (text!.isEmpty) {
      message = 'Please enter password';
    } else if (validateNumberCharacters && text.length < 5) {
      message = 'Password must be greater than 8 characters';
    } else if (validateSpecial && !validateString(text)) {
      message = 'Use at least one letter, number & special  character';
    }

    return message;
  }

  static String? validateName(String? text) {
    String? message;
    if (text!.isEmpty || text.length < 3) {
      message = 'Please enter name';
    }

    return message;
  }
  void launchPrivacyUrl() async {
    if (!await launchUrl(url)) throw 'Could not launch $url';
  }

  void launchTermUrl() async {
    if (!await launchUrl(url2)) throw 'Could not launch $url2';
  }

// }
}

class Helper {
  static bool emailRegExp(String text) {
    RegExp regex = RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
    return regex.hasMatch(text);
  }
}
