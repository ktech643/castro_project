import 'package:castro_app/screens/StartingScreens/splash_screen.dart';
import 'package:castro_app/services/update_fcm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  if (message.notification != null) {}
}

FirebaseServices firebaseServices = FirebaseServices();
final GlobalKey<NavigatorState> navState = GlobalKey<NavigatorState>();

firebaseInit() async {
  await Firebase.initializeApp();
  FirebaseMessaging _firebaseMessaging = await FirebaseMessaging.instance;
  _firebaseMessaging.getToken().then((token) async {
    firebaseServices.updateFcmToken(
      fcm_Token: token.toString(),
    );

    NotificationSettings settings = await _firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: true,
      sound: true,
    );
    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
    } else {}

    FirebaseMessaging.onMessage.listen((
      RemoteMessage message,
    ) {
      if (message.notification != null) {}
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      if (message.notification != null) {
        // navState.currentState.pushNamed(NotificationScreen.id);
      }
    });
  });
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  firebaseInit();
  runApp(MaterialApp(builder: EasyLoading.init(), home: SplashScreen()));
}
