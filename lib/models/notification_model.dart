
class NotificationModel {
  int? id;
  String? customer_id;
  String? professional_user_id;
  String? send_by_user_id;
  String? body;
  String? status;

  NotificationModel({
    this.id,
    this.customer_id,
    this.professional_user_id,
    this.send_by_user_id,
    this.body,
    this.status,
  });

  factory NotificationModel.fromJson(Map<String, dynamic> json) {
    return NotificationModel(
      id: json['id'],
      customer_id: json['customer_id'],
      professional_user_id: json['professional_user_id'],
      send_by_user_id: json['send_by_user_id'],
      body: json['body'],
      status: json['status'],
    );
  }
}
