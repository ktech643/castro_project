class ProfessionalUserModel {
  int? id;
  String? name;
  String? phone;
  String? address;
  int? speciality_id;
  String? experience;
  String? image;
  String? price;
  String? email;
  String? dob;
  int? reviews_count;
   dynamic reviews_avg;
  String? zip_code;
  String? latitude;
  String? longitude;
  final List<SubServicesModel>? subservices;
  final List<ReviewsModel>? reviewModel;

  ProfessionalUserModel({
    this.id,
    this.name,
    this.phone,
    this.address,
    this.speciality_id,
    this.experience,
    this.image,
    this.reviews_count,
    this.reviews_avg,
    this.price,
    this.email,
    this.dob,
    this.zip_code,
    this.latitude,
    this.longitude,
    this.subservices,
    this.reviewModel,
  });

  factory ProfessionalUserModel.fromJson(Map<String, dynamic> json) {
    return ProfessionalUserModel(
      id: json['id'],
      name: json['name'],
      phone: json['phone'],
      address: json['address'],
      speciality_id: json['speciality_id'] != null
          ? int.parse(json['speciality_id'])
          : null,
      experience: json['experience'],
      image: json['image'],
      price: json['price'],
      email: json['email'],
      reviews_count: json['reviews_count'],
      reviews_avg: json['reviews_avg'],
      dob: json['dob'],
      zip_code: json['zip_code'],
      latitude: json['latitude'],
      longitude: json['longitude'],
      subservices:
          json.containsKey('subservices') && json['subservices'].length > 0
              ? json['subservices']
                  .map<SubServicesModel>(
                      (serviceModel) => SubServicesModel.fromJson(serviceModel))
                  .toList()
              : [],
      reviewModel: json.containsKey('reviews') && json['reviews'].length > 0
          ? json['reviews']
              .map<ReviewsModel>(
                  (reviewModel) => ReviewsModel.fromJson(reviewModel))
              .toList()
          : [],
    );
  }
}

class SubServicesModel {
  final int? id;
  final String? name;
  final String? description;
  final String? price;

  SubServicesModel({
    this.id,
    this.name,
    this.description,
    this.price,
  });

  factory SubServicesModel.fromJson(Map<String, dynamic> json) {
    return SubServicesModel(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      price: json['price'],
    );
  }
}

class ReviewsModel {
  final String? id;
  final String? profesional_user_id;
  final String? user_id;
  final String? rating;
  final String? review;
  final String? user_name;

  ReviewsModel({
    this.id,
    this.profesional_user_id,
    this.user_id,
    this.rating,
    this.review,
    this.user_name,
  });

  factory ReviewsModel.fromJson(Map<String, dynamic> json) {
    return ReviewsModel(
      id: json['id'],
      profesional_user_id: json['profesional_user_id'],
      user_id: json['user_id'],
      rating: json['rating'],
      review: json['review'],
      user_name: json['user_name'],
    );
  }
}
