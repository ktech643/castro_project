class SpecialtiesModel {
  final int id;
  final String name;

  SpecialtiesModel({
    required this.id,
    required this.name,
  });

  factory SpecialtiesModel.fromJson(Map<String, dynamic> json) {
    return SpecialtiesModel(
      id: json['id'],
      name: json['name'],
    );
  }
}
