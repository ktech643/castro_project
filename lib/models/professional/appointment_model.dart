import 'package:castro_app/models/professional/professional_user_model.dart';

class AppointmentModel {
  int? id;
  String? customer_id;
  String? professional_user_id;
  String? booking_date;
  String? booking_time;
  dynamic total_amount;
  String? status;
  ProfessionalUserModel? customer_user;
  List<SubServicesModel>? booked_sub_services;

  AppointmentModel({
    this.id,
    this.customer_id,
    this.professional_user_id,
    this.booking_date,
    this.booking_time,
    this.status,
    this.customer_user,
    this.total_amount,
    this.booked_sub_services,
  });

  factory AppointmentModel.fromJson(Map<String, dynamic> json) {
    return AppointmentModel(
        id: json['id'],
        customer_id: json['customer_id'],
        professional_user_id: json['professional_user_id'],
        booking_date: json['booking_date'],
        booking_time: json['booking_time'],
        status: json['status'],
        total_amount: json['total_amount'],
        customer_user: ProfessionalUserModel.fromJson(json['customer_user']),
        booked_sub_services: json['booked_sub_services']
            .map<SubServicesModel>((serviceModel) =>
                SubServicesModel.fromJson(serviceModel['sub_service']))
            .toList());
  }
}
