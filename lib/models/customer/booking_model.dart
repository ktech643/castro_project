import 'package:castro_app/models/professional/specialities_list.dart';

class BookingModel {
  int? id;
  String? customer_id;
  String? professional_user_id;
  String? booking_date;
  int? total_price;
  String? status;
  ProfessionalBookingModel? professional_user;

  BookingModel({
    this.id,
    this.customer_id,
    this.total_price,
    this.professional_user_id,
    this.booking_date,
    this.status,
    this.professional_user,
  });

  factory BookingModel.fromJson(Map<String, dynamic> json) {
    return BookingModel(
      id: json['id'],
      customer_id: json['customer_id'],
      total_price: json['total_price'],
      professional_user_id: json['professional_user_id'],
      booking_date: json['booking_date'],
      status: json['status'],
      professional_user:
          ProfessionalBookingModel.fromJson(json['professional_user']),
    );
  }
}

class ProfessionalBookingModel {
  int? id;
  String? name;
  String? phone;
  String? address;
  int? speciality_id;
  String? experience;
  String? image;
  String? price;
  String? email;
  String? dob;
  String? zip_code;
  SpecialtiesModel? speciality;

  ProfessionalBookingModel({
    this.id,
    this.name,
    this.phone,
    this.address,
    this.speciality_id,
    this.experience,
    this.image,
    this.price,
    this.email,
    this.dob,
    this.zip_code,
    this.speciality,
  });

  factory ProfessionalBookingModel.fromJson(Map<String, dynamic> json) {
    return ProfessionalBookingModel(
      id: json['id'],
      name: json['name'],
      phone: json['phone'],
      address: json['address'],
      speciality_id: json['speciality_id'] != null
          ? int.parse(json['speciality_id'])
          : null,
      experience: json['experience'],
      image: json['image'],
      price: json['price'],
      email: json['email'],
      dob: json['dob'],
      zip_code: json['zip_code'],
      speciality: json['speciality'] != null
          ? SpecialtiesModel.fromJson(json['speciality'])
          : null,
    );
  }
}
